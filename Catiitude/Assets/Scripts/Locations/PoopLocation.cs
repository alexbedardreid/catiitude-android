﻿using UnityEngine;
using System.Collections;

public class PoopLocation : Location
{
	public override void OnTriggerEnter2D(Collider2D other)
	{
		base.OnTriggerEnter2D(other);

		if(other.tag == "Poop") Complete();
	}

	public override void OnTriggerStay2D(Collider2D other)
	{
		base.OnTriggerStay2D(other);
	}

	void Complete()
	{
		if (m_runOnce && m_ran) return;
		
		Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
		m_cat.m_catiitudePoints += m_pointsAwarded;
		m_cat.AddScore(m_pointsAwarded);

		Color c = m_renderer.color;
		c.a = 0;
		m_renderer.color = c;

		World.IncrementAchievement(GameManaging.GameManager.ACH_POOP_MASTER, 1);// Poop Master

		if (m_runOnce)
		{
			m_ran = true;
			FadeOutIcon();
		}
	}
}
