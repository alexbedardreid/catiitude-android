﻿using UnityEngine;
using System.Collections;

using GameManaging;

public class EatLocation : Location
{

	public override void OnTriggerEnter2D(Collider2D other)
	{
		base.OnTriggerEnter2D(other);
	}
	
	public override void OnTriggerStay2D(Collider2D other)
	{
		base.OnTriggerStay2D(other);

		if(!m_actionAwaitingProcessing) return;

		if(m_actionName == "Eat" && other.gameObject == m_cat.gameObject)
			Complete();

		/*if(m_runOnce)
		{
			if(!m_ran)
			{
				if(m_actionAwaitingProcessing)
				{
					m_actionAwaitingProcessing = false;
					
					if(m_actionName == "Eat")
					{
						if(other.gameObject == m_cat.gameObject)
						{
							// Insert Run Once Actions Here
							Poop poo = (Poop)m_cat.GetAction("Poop");
							poo.Full = true;
							
							if(m_runOnce)
							{
								m_ran = true;
								FadeOutIcon();
							}
						}
					}
				}
			}
		}
		else
		{
			if(m_actionAwaitingProcessing)
			{
				m_actionAwaitingProcessing = false;
				
				if(m_actionName == "Eat")
				{
					if(other.gameObject == m_cat.gameObject)
					{
						// Insert Actions Here
						Poop poo = (Poop)m_cat.GetAction("Poop");
						poo.Full = true;
						UIManager.Instance.FillPoopSprite(poo.m_timerDuration);
					}
				}
			}
		}*/
	}

	void Complete()
	{
		foreach (EatLocation _ovr in World.GetLocations(typeof(EatLocation)))
			_ovr.m_actionAwaitingProcessing = false;

		if (m_runOnce && m_ran) return;

		// Insert Run Once Actions Here
		Poop poo = (Poop)m_cat.GetAction(ACTION.POOP);
		poo.Full = true;
							
		if(m_runOnce)
		{
			m_ran = true;
			FadeOutIcon();
		}
	}
	
}
