﻿using UnityEngine;
using System.Collections;



public class Location : MonoBehaviour
{
	public int m_pointsAwarded = 1;

	public bool m_runOnce = false;
	public bool m_ran = false;

	public bool m_special = false;

	World m_World;
	protected World m_world{ get{if(!m_World)m_World = World.Instance; return m_World;} set{m_World = value;} }

	Cat m_Cat;
	protected Cat m_cat{ get{if(!m_Cat)m_Cat = m_world._cat; return m_Cat;} set{m_Cat = value;} }
	
	protected string m_actionName;
	protected bool m_actionAwaitingProcessing = false;

	protected SpriteRenderer m_renderer;

	protected bool soonActive = false;

	void Awake()
	{
		Transform  find;
		if (find = this.transform.Find("Icon"))
			m_renderer = find.GetComponent<SpriteRenderer>();
		else gameObject.SetActive(false);
	}

	void Update()
	{

	}

	public virtual void OnTriggerEnter2D(Collider2D other)
	{
		if(!m_ran)
		{
			if(other.tag == "Cat")
			{
				FadeInIcon();
				soonActive = true;
			}
		}
		else
		{
			Color c = m_renderer.color;
			c.a = 0;
			m_renderer.color = c;
		}
	}

	public virtual void OnTriggerStay2D(Collider2D other)
	{

	}

	public virtual void OnTriggerExit2D(Collider2D other)
	{
		if(!m_ran)
		{
			if(other.tag == "Cat")
			{
				FadeOutIcon();
				soonActive = false;
			}
		}
		else
		{
			Color c = m_renderer.color;
			c.a = 0;
			m_renderer.color = c;
		}
	}

	protected void FadeInIcon()
	{ StartCoroutine(fadeInIcon()); }
	IEnumerator fadeInIcon()
	{
		float _i = 0f;
		Color c = m_renderer.color;

		while(_i< 1f)
		{
			m_renderer.color = Color.Lerp(Color.clear, Color.white, (_i += Time.deltaTime * 3f));
			yield return null;
		}
	}

	protected void FadeOutIcon()
	{ StartCoroutine(fadeOutIcon()); }
	IEnumerator fadeOutIcon()
	{
		float _i = 0f;
		Color c = m_renderer.color;
		
		while(_i < 1f)
		{
			m_renderer.color = Color.Lerp(Color.white, Color.clear, (_i += Time.deltaTime * 3f));
			yield return null;
		}
	}

	public void ActionNotification(string l_actionName)
	{
		if(!soonActive) return;

		m_actionName = l_actionName;
		m_actionAwaitingProcessing = true;
	}
}
