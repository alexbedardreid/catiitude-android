﻿using UnityEngine;
using System.Collections;

public class SitLocation : Location
{
	public override void OnTriggerEnter2D(Collider2D other)
	{
		base.OnTriggerEnter2D(other);
	}
	
	public override void OnTriggerStay2D(Collider2D other)
	{
		base.OnTriggerStay2D(other);

		if (!m_actionAwaitingProcessing) return;

		if (m_actionName == "Sit" && other.gameObject == m_cat.gameObject)
			Complete();
	}

	void Complete()
	{
		foreach (SitLocation _ovr in World.GetLocations(typeof(SitLocation)))
			_ovr.m_actionAwaitingProcessing = false;

		if (m_runOnce && m_ran) return;

		Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
		m_cat.m_catiitudePoints += m_pointsAwarded;
		m_cat.AddScore(m_pointsAwarded);

		//TODO Check for sitting on head
		if (PlayerPrefs.GetInt("LittleComforts", 0) == 0)
		{
			World.UnlockAchievement(GameManaging.GameManager.ACH_LITTLE_COMFORTS);// Little Comforts
			PlayerPrefs.SetInt("LittleComforts", 1);
			PlayerPrefs.Save();
		}
		else
			World.IncrementAchievement(GameManaging.GameManager.ACH_A_COMFORTABLE_BUTT, 1);//A Comfortable Butt

		if (m_special) World.UnlockAchievement(GameManaging.GameManager.ACH_THE_GRAND_MASTER_OF_HOOMANS);// Grand Master Hooman

		if (m_runOnce)
		{
			m_ran = true;
			FadeOutIcon();
		}
	}

}
