﻿using UnityEngine;
using System.Collections;

public class KnockOver : Location
{
	public Rigidbody2D m_objectToKnockOver;

	public Vector2 m_force = new Vector2(100.0f,0.0f);
	public float m_torque = 15.0f;

	public override void OnTriggerEnter2D(Collider2D other)
	{
		base.OnTriggerEnter2D(other);
	}
	
	public override void OnTriggerStay2D(Collider2D other)
	{
		base.OnTriggerStay2D(other);

		if (!m_actionAwaitingProcessing) return;

		if ((m_actionName == "Attack" || Input.GetKeyDown(KeyCode.Space)) && other.gameObject == m_cat.gameObject)
			Complete();
		
		/*if(m_runOnce)
		{
			if(!m_ran)
			{
				if(m_actionAwaitingProcessing)
				{
					if (m_actionName == "Attack" || Input.GetKeyDown(KeyCode.Space))
					{
						if(other.gameObject == m_cat.gameObject)
						{
							foreach(KnockOver _ovr in World.GetLocations(typeof(KnockOver)))
								_ovr.m_actionAwaitingProcessing = false;

							Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
							m_cat.m_catiitudePoints += m_pointsAwarded;
							m_cat.AddScore(m_pointsAwarded);

                            if (m_cat.m_facingRight)
                            {
                                m_objectToKnockOver.AddForce(m_force);
                                m_objectToKnockOver.AddTorque(m_torque);
                            }
                            else
                            {
								m_objectToKnockOver.AddForce(new Vector2((m_force.x * -1), m_force.y));
                                m_objectToKnockOver.AddTorque(m_torque * -1f);
                            }

							if(m_runOnce)
							{
								m_ran = true;
								FadeOutIcon();
							}
						}
					}
				}
			}
		}
		else
		{
			if(m_actionAwaitingProcessing)
			{
				if (m_actionName == "Attack" || Input.GetKeyDown(KeyCode.Space))
				{
					if(other.gameObject == m_cat.gameObject)
					{
						foreach(KnockOver _ovr in World.GetLocations(typeof(KnockOver)))
							_ovr.m_actionAwaitingProcessing = false;

						Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
						m_cat.m_catiitudePoints += m_pointsAwarded;
						m_cat.AddScore(m_pointsAwarded);

                        if (m_cat.m_facingRight)
                        {
                            m_objectToKnockOver.AddForce(m_force);
                            m_objectToKnockOver.AddTorque(m_torque);
                        }
                        else
                        {
							m_objectToKnockOver.AddForce(new Vector2((m_force.x * -1), m_force.y));
                            m_objectToKnockOver.AddTorque(m_torque * -1f);
                        }



						
					}
				}
			}
		}*/
	}

	void SetIsKinematic()
	{
		m_objectToKnockOver.isKinematic = true;
	}

	void Complete()
	{
		foreach (KnockOver _ovr in World.GetLocations(typeof(KnockOver)))
			_ovr.m_actionAwaitingProcessing = false;

		if (m_runOnce && m_ran) return;

		Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
		m_cat.m_catiitudePoints += m_pointsAwarded;
		m_cat.AddScore(m_pointsAwarded);
		//TODO Increment the Achievement

		if (m_special)
		{
			transform.parent.GetComponent<SpringJoint2D>().enabled = false;
			transform.parent.GetComponent<Fly>().Stop();
			m_objectToKnockOver.gravityScale = 1f;
			m_objectToKnockOver.mass = 1f;
		}

		if (PlayerPrefs.GetInt("PureAnarchy", 0) == 0)
		{
			World.UnlockAchievement(GameManaging.GameManager.ACH_PURE_ANARCHY);// Pure Anarchy Achievement
			PlayerPrefs.SetInt("PureAnarchy", 1);
			PlayerPrefs.Save();
		}
		else
			World.IncrementAchievement(GameManaging.GameManager.ACH_YOURE_AN_ADULT, 1);//You're an Adult Achievement

		if (m_cat.m_facingRight)
		{
			m_objectToKnockOver.AddForce(m_force);
			m_objectToKnockOver.AddTorque(m_torque);
		}
		else
		{
			m_objectToKnockOver.AddForce(new Vector2((m_force.x * -1), m_force.y));
			m_objectToKnockOver.AddTorque(m_torque * -1f);
		}

		

		if (m_runOnce)
		{
			m_ran = true;
			FadeOutIcon();
		}
	}
}
