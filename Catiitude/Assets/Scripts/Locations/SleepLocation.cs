﻿using UnityEngine;
using System.Collections;

public class SleepLocation : Location
{
	public override void OnTriggerEnter2D(Collider2D other)
	{
		base.OnTriggerEnter2D(other);
	}
	
	public override void OnTriggerStay2D(Collider2D other)
	{
		base.OnTriggerStay2D(other);

		if (!m_actionAwaitingProcessing) return;

		if (m_actionName == "Sleep" && other.gameObject == m_cat.gameObject)
			Complete();

		/*
		if(m_runOnce)
		{
			if(!m_ran)
			{
				if(m_actionAwaitingProcessing)
				{
					foreach(SleepLocation _ovr in World.GetLocations(typeof(SleepLocation)))
						_ovr.m_actionAwaitingProcessing = false;
					
					if(m_actionName == "Sleep")
					{
						if(other.gameObject == m_cat.gameObject)
						{
							Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
							m_cat.m_catiitudePoints += m_pointsAwarded;
							m_cat.AddScore(m_pointsAwarded);
							
							if(m_runOnce)
							{
								m_ran = true;
								FadeOutIcon();
							}
						}
					}
				}
			}
		}
		else
		{
			if(m_actionAwaitingProcessing)
			{
				foreach(SleepLocation _ovr in World.GetLocations(typeof(SleepLocation)))
					_ovr.m_actionAwaitingProcessing = false;
				
				if(m_actionName == "Sleep")
				{
					if(other.gameObject == m_cat.gameObject)
					{
						Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
						m_cat.m_catiitudePoints += m_pointsAwarded;
						m_cat.AddScore(m_pointsAwarded);
					}
				}
			}
		}*/
	}

	void Complete()
	{
		foreach (SleepLocation _ovr in World.GetLocations(typeof(SleepLocation)))
			_ovr.m_actionAwaitingProcessing = false;

		if (m_runOnce && m_ran) return;

		Sounds.SoundManager.PlaySound(Sounds.SOUNDS.COLCT);
		m_cat.m_catiitudePoints += m_pointsAwarded;
		m_cat.AddScore(m_pointsAwarded);

		if (m_runOnce)
		{
			m_ran = true;
			FadeOutIcon();
		}
	}
}
