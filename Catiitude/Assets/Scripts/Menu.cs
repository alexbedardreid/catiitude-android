﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.SocialPlatforms;

using GooglePlayGames;
using GooglePlayGames.BasicApi;

using GameManaging;

public class Menu : MonoBehaviour
{
#if UNITY_EDITOR
	public bool debug = true;
#else
	bool debug = false;
#endif
	[Space(15)]
	public GameObject m_catLeft;
	public GameObject m_catRight;
	public GameObject m_paws;

	[Space(15)]
	public List<GameObject> MenusUI = new List<GameObject>();
	/////////////////////////////////////////////////////////
	////////////////MENUS UI LIST POSITIONS//////////////////
	/////////////////////////////////////////////////////////
	//0 - Main Menu
	//1 - Levels Menu
	//99 - 

	[Space(15)]
	public LevelUIManager levelUIManager = new LevelUIManager();
	
	[Space(15)]
	public float m_catDistanceMax = 8.0f;
	public float m_catDistanceMin = 2.0f;

	public float m_transitionTime = 1.0f;
	private float m_currentTransitionTime = 0.0f;

	private bool m_catLeftMoving = true;
	private bool m_movingIn = true;

	float amplitudeX = 90f;
	float amplitudeY = 27f;
	float omegaX = 10f;
	float omegaY = 20f;
	float index;

	void Start()
	{
		Application.targetFrameRate = 120;
		OpenUIMenu(0);//Just incase its not open

#if UNITY_ANDROID
		PlayGamesPlatform.Activate();

		PlayGamesPlatform.Instance.Authenticate((bool success) =>
		{
			//SocialLabel.text = (success ? Social.localUser.userName + " Connected" : "Social: Failed");

		});

#endif
		levelUIManager.Init();

	}
	#region Main Update Loop
	void Update()
	{
		//Cats
		if(m_catLeftMoving)
		{
			m_currentTransitionTime += Time.deltaTime;

			if(m_movingIn)
			{
				m_catLeft.transform.localPosition = Vector3.Lerp(new Vector3(-m_catDistanceMax,m_catLeft.transform.localPosition.y,0),
				                                            new Vector3(-m_catDistanceMin,m_catLeft.transform.localPosition.y,0),
				                                            m_currentTransitionTime/m_transitionTime);

				if(m_currentTransitionTime >= m_transitionTime)
				{
					m_movingIn = false;
					m_currentTransitionTime = 0.0f;
				}
			}
			else
			{
				m_catLeft.transform.localPosition = Vector3.Lerp(new Vector3(-m_catDistanceMin,m_catLeft.transform.localPosition.y,0),
				                                                 new Vector3(-m_catDistanceMax,m_catLeft.transform.localPosition.y,0),
				                                            m_currentTransitionTime/m_transitionTime);

				if(m_currentTransitionTime >= m_transitionTime)
				{
					m_movingIn = true;
					m_currentTransitionTime = 0.0f;
					m_catLeftMoving = false;
				}
			}
		}
		else
		{
			m_currentTransitionTime += Time.deltaTime;
			
			if(m_movingIn)
			{
				m_catRight.transform.localPosition = Vector3.Lerp(new Vector3(m_catDistanceMax,m_catRight.transform.localPosition.y,0),
				                                                  new Vector3(m_catDistanceMin,m_catRight.transform.localPosition.y,0),
				                                                 m_currentTransitionTime/m_transitionTime);
				
				if(m_currentTransitionTime >= m_transitionTime)
				{
					m_movingIn = false;
					m_currentTransitionTime = 0.0f;
				}
			}
			else
			{
				m_catRight.transform.localPosition = Vector3.Lerp(new Vector3(m_catDistanceMin,m_catRight.transform.localPosition.y,0),
				                                                  new Vector3(m_catDistanceMax,m_catRight.transform.localPosition.y,0),
				                                                 m_currentTransitionTime/m_transitionTime);
				
				if(m_currentTransitionTime >= m_transitionTime)
				{
					m_movingIn = true;
					m_currentTransitionTime = 0.0f;
					m_catLeftMoving = true;
				}
			}


		}
	#endregion

		// Paws
		index += Time.deltaTime * 0.0125f;
		float x = amplitudeX * Mathf.Cos(omegaX * index);
		float y = Mathf.Abs(amplitudeY * Mathf.Sin (omegaY * index));
		m_paws.transform.localPosition = new Vector3(x,y,0);

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.F10))
		{
			PlayerPrefs.DeleteAll();
			PlayerPrefs.Save();
		}
#endif
	}

	void OpenUIMenu(int id)
	{
		foreach (GameObject _go in MenusUI)
			_go.SetActive(false);

		MenusUI[id].SetActive(true);
	}

	#region Main Menu Buttons
	public void OnPlayPressed()
	{
		Application.LoadLevel(levelUIManager.Play());
	}
	public void OnLevelsPressed()
	{
		OpenUIMenu(1);
	}

	public void OnRatePressed()
	{
		//TODO Add the opening of the play store
	}
	public void OnAchievementsPressed()
	{
		if (Social.localUser.authenticated)
			Social.ShowAchievementsUI();
	}
	#endregion

	#region Levels Menu

	public void OnBackPressed()
	{
		//TODO Close current menus, Open Main menu
		OpenUIMenu(0);
	}

	public void OnTutorialPressed()
	{
		if (!movingLevelUI) StartCoroutine(SelectLevel(0));
	}

	public void OnLevelOnePressed()
	{
		if (!movingLevelUI) StartCoroutine(SelectLevel(1));
	}

	public void OnLevelTwoPressed()
	{
		if (!movingLevelUI) StartCoroutine(SelectLevel(2));
	}

	public void OnPrototypePressed()
	{
		Application.LoadLevel("Prototype");
	}


	public void OnLeaderboardPressed()
	{
		levelUIManager.SelectLeaderboard();
	}

	public void OnReplayPressed()
	{
		levelUIManager.ReplaySelectedLevel(MODE.NORMAL);
	}

	public void OnReplayRandomPressed()
	{
		levelUIManager.ReplaySelectedLevel(MODE.RANDOM);
	}

	//This moves the stats UI so that it will move in and out of position
	bool movingLevelUI = false;
	Vector2 top = new Vector2(0f,650f);
	Vector2 middle = new Vector2(0f,0f);
	Vector2 bottom = new Vector2(0f, -850f);
	IEnumerator SelectLevel(int id)
	{
		//Avoid running this coroutine multiple times
		if (movingLevelUI) yield break;
		else movingLevelUI = true;

		float lerp = 0f;

		//Quick access for the panel componentsz
		UIPanel panel = levelUIManager.levelStatsPanel;
		Transform panelTrans = panel.transform;

		levelUIManager.SelectLevel(id);

		while(lerp <1f)//Move UI down, and fade out
		{
			panelTrans.localPosition = Vector2.Lerp(middle, bottom, lerp += Time.deltaTime * 3f);
			panel.alpha = Mathf.Lerp(1f, 0f, lerp * 1.5f);
			yield return null;
		}

		levelUIManager.SetUIStats(id);//Sets the stats UI 
		lerp = 0f;

		while (lerp < 1f)//Moves the UI from top to center
		{
			panelTrans.localPosition = Vector2.Lerp(top, middle, lerp += Time.deltaTime * 3f);
			panel.alpha = Mathf.Lerp(0f, 1f, lerp);
			yield return null;
		}

		movingLevelUI = false;
		yield return null;
	}

	#endregion
}
#region Extra Classes

#region Levels UI
[System.Serializable]
public class LevelUIManager
{
	[Space(15)]
	public List<LevelUI> levelsUI = new List<LevelUI>();

	[Space(15)]
	public UILabel levelNameLabel;
	public UILabel pointsLabel;
	public UILabel timeLabel;

	[Space(10)]
	public UIButton replayButton;
	public UIButton RandomButton;

	[Space(10)]
	public UIPanel levelStatsPanel;

	[Space(10)]
	public UISprite HightLightSprite;
	GameObject HightLightObject;
	Transform HightLightTrans;

	int selectedLevel = -1;
	string levelName { set { levelNameLabel.text = value; } }
	int points { set { pointsLabel.text = "Points: " + value; } }
	float time { set { timeLabel.text = "Time: " + value; } }

	public void Init()
	{
		int id = 0;
		string _temp = "";

		foreach(LevelUI _lvl in levelsUI)
		{
			_temp = GameManager.Levels[id].levelSceneName;

			bool actv = (PlayerPrefs.GetInt(_temp + "Complete", 0) == 1);

			_lvl.SetActive(actv);
			_lvl.totalPoints = PlayerPrefs.GetInt(_temp + "Points", 0);
			_lvl.totalTime = (float)Math.Round(PlayerPrefs.GetFloat(_temp + "Time", 999f), 2);
			id++;
		}

		if (HightLightSprite)
		{
			HightLightObject = HightLightSprite.gameObject;
			HightLightTrans = HightLightSprite.transform;
		}

		SelectLevel(0);
		SetUIStats(0);
	}

	public string Play()
	{
		return (levelsUI[0].active ?
			GameManager.Levels[1].levelSceneName : GameManager.Levels[0].levelSceneName);
	}

	public void ReplaySelectedLevel(MODE gameMode)
	{
		if (selectedLevel < 0) return;

		string levelName = GameManager.Levels[selectedLevel].levelSceneName;

		if (levelName == "Tutorial") GameManager.CurrentMode = MODE.NORMAL;
		else GameManager.CurrentMode = gameMode;

		Application.LoadLevel(levelName);
	}

	public void SelectLevel(int id)
	{
		LevelUI temp = levelsUI[id];

		replayButton.isEnabled = temp.active;
		RandomButton.isEnabled = temp.active;

		HightLightObject.SetActive(temp.active);

		//TODO glide the highlight as well
		HightLightTrans.position = temp.Position;

		//RandomButton.gameObject.SetActive(!(GameManager.Levels[id].levelSceneName == "Tutorial"));

		if (!temp.active)
		{
			levelName = "Complete Levels";
			pointsLabel.text = "";
			timeLabel.text = "";

			return;
		}
		

	}
	public void SetUIStats(int id)
	{
		LevelUI temp = levelsUI[id];

		if (!temp.active) return;

		//TODO Add a coroutine to move the UI so it moves in nicely
		selectedLevel = id;

		RandomButton.gameObject.SetActive(!(GameManager.Levels[id].levelSceneName == "Tutorial"));

		levelName = GameManager.Levels[id].UIDisplayName;
		points = temp.totalPoints;
		time = temp.totalTime;
	}

	public void SelectLeaderboard()
	{
		if (Social.localUser.authenticated)
			PlayGamesPlatform.Instance.ShowLeaderboardUI(
				GameManager.GetLeaderboardCode(selectedLevel));
	}
}

[System.Serializable]
public class LevelUI
{
	public bool active;

	//public string sceneName;// Actual Scene Name
	//public string level;//UI Display Name

	public UIButton LevelButton;

	public Vector2 Position { get { return LevelButton.transform.position; } }

	public float totalTime;
	public int totalPoints;
	//public int levelID;

	public void SetActive(bool state)
	{
		LevelButton.isEnabled = state;
		active = state;
	}


	//TODO Replay types: Random, Time Trial, Hard?
	//TODO Stars?



}
#endregion //Levels UI

#endregion // Extra Classes
