﻿using UnityEngine;
using System.Collections;

public class Pickup : Action
{
	//Transform m_transform;

	public Pickup(string l_name, Transform l_transform) : base(l_name)
	{
		//m_transform = l_transform;
	}
	
	public Pickup(string l_name, Transform l_transform, float l_timerDuration) : base(l_name,l_timerDuration)
	{
		//m_transform = l_transform;
	}
	
	public override void Check()
	{
		base.Check();
	}
	
	public override void Run(Animator l_animator)
	{
		if(m_active)
		{
			l_animator.SetTrigger(m_actionName);
			m_active = false;
			m_world.ActionNotification(m_actionName);
		}
		
		//base.Run (l_animator);
	}
}
