﻿using UnityEngine;
using System.Collections;

public class Barf : Action
{
	private bool m_hairball = true;
	private Transform m_transform;
	
	public Barf(string l_name, Transform l_transform) : base(l_name)
	{
		m_transform = l_transform;
	}
	
	public Barf(string l_name, Transform l_transform, float l_timerDuration) : base(l_name,l_timerDuration)
	{
		m_transform = l_transform;
	}

	public bool HairBall
	{
		get { return m_hairball; }
		set { m_hairball = value; }
	}

	public override void Check()
	{
		base.Check();

		if(!m_hairball)
		{
			m_enabled = false;
			m_currentTimer = 0.0f;
		}

		m_UIManager.actions.Puke.isEnabled = m_enabled;
	}

	public override void Run(Animator l_animator)
	{
		if(m_active)
		{
			if(m_hairball)
			{
				MonoBehaviour.Instantiate(m_world.m_barfObject, m_transform.Find("BarfPoint").position,Quaternion.identity);
				m_hairball = false;
			}	
		}

		base.Run (l_animator);
	}
}
