﻿using UnityEngine;
using System.Collections;

using GameManaging;

public class Lick : Action
{	
	public Lick(string l_name) : base(l_name)
	{
	}
	
	public Lick(string l_name, float l_timerDuration) : base(l_name,l_timerDuration)
	{
	}
	
	public override void Check()
	{
		base.Check();
	}
	
	public override void Run(Animator l_animator)
	{
		if(m_active)
		{
			Barf barf = (Barf)m_cat.GetAction(ACTION.BARF);
			barf.HairBall = true;
			UIManager.Instance.FillPukeSprite(barf.m_timerDuration);
		}
		
		base.Run (l_animator);
	}
}
