﻿using UnityEngine;
using System.Collections;

public class Action
{
	public string m_actionName = "Action";
	public bool m_hasTimer = false;
	public float m_timerDuration = 1.0f;

	public bool m_enabled = true;

	public float m_currentTimer = 0.0f;
	protected bool m_active = false;

	protected int animatorHash;

    protected World _World;
    protected World m_world { get { if (!_World)_World = World.Instance; return _World; } set { _World = value; } }

    protected Cat _Cat;
    protected Cat m_cat { get { if (!_Cat)_Cat = m_world._cat; return _Cat; } set { _Cat = value; } }

	protected UIManager _uiManager;
	protected UIManager m_UIManager { get { if (!_uiManager)_uiManager = UIManager.Instance; return _uiManager; }
		set { _uiManager = value; } }

	void Awake()
	{
		m_world = World.Instance;
		m_cat = m_world._cat;

	}

	public Action(string l_name)
	{
		m_actionName = l_name;
		animatorHash = Animator.StringToHash(m_actionName);
	}
	
	public Action(string l_name, float l_timerDuration)
	{
		m_actionName = l_name;
		m_hasTimer = true;
		m_timerDuration = l_timerDuration;
		animatorHash = Animator.StringToHash(m_actionName);
	}

	public virtual void Check()
	{
		if(m_hasTimer)
		{
			if(!m_enabled)
			{
				m_currentTimer += Time.deltaTime;

				if(m_currentTimer >= m_timerDuration)
				{
					m_currentTimer = 0.0f;
					m_enabled = true;
				}
			}
			else
			{
				CheckPressed();
			}
		}
		else
		{
			CheckPressed();
		}
	}
	
	public virtual void Run(Animator l_animator)
	{
		if(m_active)
		{
			l_animator.SetTrigger(animatorHash);
			m_active = false;
			m_world.ActionNotification(m_actionName);
		}
	}

	void CheckPressed()
	{
		#if UNITY_EDITOR && UNITY_STANDALONE
		if(Input.GetButtonDown(m_actionName))
		{
			m_enabled = false;
			m_active = true;
		}
		#elif UNITY_ANDROID
		if(World.Instance.actionPressed[m_actionName])
		{
			if(!m_enabled)
			{
				World.Instance.actionPressed[m_actionName] = false;
				return;
			}

			m_enabled = false;
			m_active = true;

			World.Instance.actionPressed[m_actionName] = false;
		}
		#endif
	}

}
