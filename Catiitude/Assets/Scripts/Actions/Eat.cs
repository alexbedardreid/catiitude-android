﻿using UnityEngine;
using System.Collections;

using GameManaging;

public class Eat : Action
{
	public Eat(string l_name) : base(l_name)
	{
	}
	
	public Eat(string l_name, float l_timerDuration) : base(l_name,l_timerDuration)
	{
	}
	
	public override void Check()
	{
		base.Check();
	}
	
	public override void Run(Animator l_animator)
	{
		if(m_active)
		{
			Poop poop = (Poop)m_cat.GetAction(ACTION.POOP);

			UIManager.Instance.FillPoopSprite(poop.m_timerDuration);
		}
		
		base.Run (l_animator);
	}
}