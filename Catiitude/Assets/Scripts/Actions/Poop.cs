﻿using UnityEngine;
using System.Collections;

public class Poop : Action
{
	private bool m_full = true;
	private Transform m_transform;

	public Poop(string l_name, Transform l_transform) : base(l_name)
	{
		m_transform = l_transform;
	}
	
	public Poop(string l_name, Transform l_transform, float l_timerDuration) : base(l_name,l_timerDuration)
	{
		m_transform = l_transform;
	}

	public bool Full
	{
		get { return m_full; }
		set { m_full = value; }
	}

	public override void Check()
	{
		base.Check();

		if(!m_full)
		{
			m_enabled = false;
			m_currentTimer = 0.0f;
		}

		m_UIManager.actions.Poop.isEnabled = m_enabled;
	}
	
	public override void Run(Animator l_animator)
	{
		if(m_active)
		{
			if(m_full)
			{
				MonoBehaviour.Instantiate(m_world.m_poopObject,m_transform.Find("PoopPoint").position,Quaternion.identity);
				m_full = false;
			}
		}

		base.Run (l_animator);
	}
}
