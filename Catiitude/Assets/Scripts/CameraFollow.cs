﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
	public float m_xMargin = 1f;
	public float m_yMargin = 1f;
	public float m_xSmooth = 8f;
	public float m_ySmooth = 8f;
	public Vector2 m_maxPosition;
	public Vector2 m_minPosition;
	
	public Transform m_cat;


	void Start ()
	{
		m_cat = World.Instance.m_catTransform;
	}


	bool CheckXMargin()
	{
		return Mathf.Abs(transform.position.x - m_cat.position.x) > m_xMargin;
	}


	bool CheckYMargin()
	{
		return Mathf.Abs(transform.position.y - m_cat.position.y) > m_yMargin;
	}


	void FixedUpdate ()
	{
		TrackPlayer();
	}
	
	
	void TrackPlayer ()
	{
		float targetX = transform.position.x;
		float targetY = transform.position.y;

		if(CheckXMargin())
		{
			targetX = Mathf.Lerp(transform.position.x, m_cat.position.x, m_xSmooth * Time.deltaTime);
		}

		if(CheckYMargin())
		{
			targetY = Mathf.Lerp(transform.position.y, m_cat.position.y, m_ySmooth * Time.deltaTime);
		}

		targetX = Mathf.Clamp(targetX, m_minPosition.x, m_maxPosition.x);
		targetY = Mathf.Clamp(targetY, m_minPosition.y, m_maxPosition.y);

		transform.position = new Vector3(targetX, targetY, transform.position.z);
	}
}
