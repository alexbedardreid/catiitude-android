﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class TutorialTrigger : MonoBehaviour 
{
	TutorialController controller;
	public int TriggerID = 0;

	void Start()
	{
		if (!(controller = World.Instance.GetComponent<TutorialController>()))
			throw new NullReferenceException("No Tutorial Controller Found.");
	}

	public virtual void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Cat")
		{
			controller.TriggerHandler(TriggerID);
			this.gameObject.SetActive(false);
		}

	}
}
