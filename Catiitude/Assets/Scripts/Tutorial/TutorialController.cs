﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TutorialController : MonoBehaviour
{
	#region Structs
	[System.Serializable]
	public struct MovementArrows
	{
		public GameObject Left;
		public GameObject Right;
		public GameObject Up;
		public GameObject Down;

		public void ToggleAll(bool state)
		{
			Left.SetActive(state);
			Right.SetActive(state);

			Up.SetActive(state);
			Down.SetActive(state);
		}
		
	}

	[System.Serializable]
	public struct ActionButton
	{
		public GameObject ButtonObject;
		public Transform ButtonTrans { get { return ButtonObject.transform; } }

		public void SetActive(bool state)
		{
			ButtonObject.SetActive(state);
		}
	}

	[System.Serializable]
	public struct Actions
	{
		public ActionButton Eat;
		public ActionButton Poop;

		public ActionButton Lick;
		public ActionButton Puke;

		public ActionButton Sit;
		public ActionButton Sleep;

		public ActionButton Hit;

		public void ToggleAll(bool state)
		{
			Eat.ButtonObject.SetActive(state);
			Poop.ButtonObject.SetActive(state);

			Lick.ButtonObject.SetActive(state);
			Puke.ButtonObject.SetActive(state);

			Sit.ButtonObject.SetActive(state);
			Sleep.ButtonObject.SetActive(state);

			Hit.ButtonObject.SetActive(state);
		}
	}
	#endregion

	public GameObject UIRoot;

	public List<GameObject> actionBarriers = new List<GameObject>();
	public List<SpriteRenderer> arrowDirections = new List<SpriteRenderer>();

	public Actions actions;
	public MovementArrows arrows;

	public Transform ActionsTransform;

	public UILabel scoreLabel;

	World _world;

	bool[] triggered = new bool[10]
	{
		false, // Trigger 1
		false, // Trigger 2
		false, // Trigger 3
		false, // Trigger Actions
		false, // Trigger Test Area
		false, // Tigger6 Poop
		false, // Tigger7 Puke
		false, // Tigger8 Sit
		false, // Tigger9 Sleep
		false
	};
	Dictionary<string, bool> actionPressed = new Dictionary<string, bool>();

	void Start()
	{
		_world = World.Instance;
		actions.ToggleAll(false);
		arrows.ToggleAll(false);

		arrows.Right.SetActive(true);

		actionPressed.Add("Barf", false);
		actionPressed.Add("Poop", false);

		actionPressed.Add("Attack", false);

		actionPressed.Add("Eat", false);

		actionPressed.Add("Bite", false);
		actionPressed.Add("Hiss", false);
		actionPressed.Add("Stare", false);
		actionPressed.Add("KnockOver", false);
		actionPressed.Add("Sit", false);

		actionPressed.Add("Ignore", false);
		actionPressed.Add("Sleep", false);
		actionPressed.Add("Lick", false);

		scoreLabel.gameObject.SetActive(false);
	}

	public void TriggerHandler(int id)
	{
		switch(id)
		{
			case 0:
				Debug.LogError("Trigger ID not set.");
				return;
			case 1:	Trigger1();	return;
			case 2:	Trigger2();	return;
			case 3: Trigger3();	return;

			case 4: TriggerActions(); return;

			case 5: TriggerTestArea(); return;

			case 6:
			case 7:
			case 8:
			case 9:
				Triggered(id);
				return;

			default: throw new NotImplementedException("Tutorial Trigger Handler not Implemented.");
		}
	}

	public void SetActionActive(string action)
	{
		actionPressed[action] = true;
	}

	#region Triggers For Arrows
	void Triggered(int id)
	{
		if (triggered[id]) return;
		else triggered[id] = true;
	}

	void Trigger1()
	{
		if (triggered[0]) return;
		else triggered[0] = true;

		arrows.Left.SetActive(true);
	}

	void Trigger2()
	{
		if (triggered[1]) return;
		else triggered[1] = true;

		arrows.Up.SetActive(true);
	}
	void Trigger3()
	{
		if (triggered[2]) return;
		else triggered[2] = true;

		arrows.Down.SetActive(true);
	}
	#endregion //Triggers For Arrows

	#region Triggers For Actions

	void TriggerActions()
	{
		if (triggered[3]) return;
		else triggered[3] = true;

		StartCoroutine(ActionsTriggered());
		//TODO detect Eating, Activate Poop
	}

	IEnumerator ActionsTriggered()
	{
		
		//Starts up the buttons to avoid scaling issues
		yield return StartCoroutine(PopActions());

		yield return StartCoroutine(WaitForTrigger("Eat", actions.Eat.ButtonObject,0));

		yield return StartCoroutine(WaitForTrigger("Poop", actions.Poop.ButtonObject,1,6));

		yield return StartCoroutine(WaitForTrigger("Lick", actions.Lick.ButtonObject,-1));

		yield return StartCoroutine(WaitForTrigger("Barf", actions.Puke.ButtonObject,2,7));

		yield return StartCoroutine(WaitForTrigger("Sit", actions.Sit.ButtonObject,3,8));

		yield return StartCoroutine(WaitForTrigger("Sleep", actions.Sleep.ButtonObject,4,9));

		actions.ToggleAll(false);
	}

	IEnumerator WaitForTrigger(string action, GameObject _go, int index, int triggerID = 0)
	{
		yield return new WaitForSeconds(0.5f);
		actions.ToggleAll(false);

		StartCoroutine(MoveArrow(index));

		if (triggerID > 0)
		{
			while (!triggered[triggerID])
			{
				yield return null;
			}
		}

		yield return StartCoroutine(GrowButton(_go));

		while (!actionPressed[action])
		{
			yield return null;
		}

		if (index >= 0) actionBarriers[index].SetActive(false);
	}

	IEnumerator GrowButton(GameObject button)
	{
		float lerp = 0f;
		Transform buttTrans = button.transform;
		Vector2 from = new Vector2(2f,2f);
		Vector2 to = new Vector2(1f,1f);
		
		button.SetActive(true);
		buttTrans.localScale = from;

		while(lerp < 1f)
		{
			buttTrans.localScale = Vector2.Lerp(from, to, lerp += Time.deltaTime);

			yield return null;
		}

	}	

	IEnumerator PopActions()
	{
		if (!ActionsTransform) yield break;
		Vector2 Originalposition = ActionsTransform.localPosition;

		ActionsTransform.localPosition = new Vector2(9999f, 9999f);

		actions.ToggleAll(true);
		yield return null;
		actions.ToggleAll(false);

		ActionsTransform.localPosition = Originalposition;
	}

	IEnumerator MoveArrow(int index)
	{

		float lerp = 0f;
		bool reverse = false;

		if (index >= 0)
		{
			Transform trans = arrowDirections[index].transform;
			Vector2 start = trans.localPosition;
			Vector2 end = new Vector2(trans.localPosition.x + .5f, trans.localPosition.y);

			//Once test Starts dont worry about moving these arrows
			while (triggered[4] == false)
			{
				if (!reverse)
				{
					trans.localPosition = Vector2.Lerp(start, end, lerp += Time.deltaTime);

					if (lerp >= 1f)
					{
						reverse = true;
					}
				}
				else
				{
					trans.localPosition = Vector2.Lerp(start, end, lerp -= Time.deltaTime);
					if (lerp <= 0f) reverse = false;
				}
				yield return null;
			}
		}
	}


	#endregion //Triggers For Actions

	#region Test Area Trigger

	void TriggerTestArea()
	{
		if (triggered[4]) return;
		else triggered[4] = true;

		scoreLabel.gameObject.SetActive(true);
		actions.ToggleAll(true);
		arrows.ToggleAll(true);
	}

	#endregion //Test Area Trigger



}
