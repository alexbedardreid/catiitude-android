﻿using UnityEngine;
using System;
using System.Collections;

public class UIManager : MonoBehaviour 
{
	[System.Serializable]
	public struct ActionButtons
	{
		public UIButton Poop;
		public UIButton Puke;
		public UIButton Hit;
		public UIButton Sit;
		public UIButton Sleep;
		public UIButton Eat;
		public UIButton Lick;

	}

	public ActionButtons actions;

	[Space(10)]
	public UISprite PoopFill;
	public UISprite PukeFill;

	[Space(10)]
	public UIButton ContinueButton;
	public UILabel PointsLabel;

	[Space(10)]
	public Camera MainCamera;
	public Camera UICamera;
	public GameObject HudText;
	public GameObject PopScore;

	public UILabel TimeLabel;
	public static float time { set { if (instance.TimeLabel)instance.TimeLabel.text = "Time: " + value; } }
	#region Script Instance
	private static UIManager instance = null;
	
	private UIManager() { }
	
	public static UIManager Instance
	{
		get
		{
			return instance;
		}
	}
	
	void Awake()
	{
		if (UIManager.Instance && !(UIManager.Instance == this))
			gameObject.SetActive(false);
		else
			instance = this;
		
	}
	
	#endregion


	public HUDText CreateFloatingText(Transform transform)
	{
		GameObject temp = (GameObject) Instantiate(HudText);
		UIFollowTarget text = temp.GetComponent<UIFollowTarget>();
		temp.name = transform.name+"_Text";

		text.gameCamera = MainCamera;
		text.uiCamera = UICamera;
		text.target = transform;

		return temp.GetComponent<HUDText>();

	}

	public void CreatePoppingScore(Transform transform, int score)
	{
		GameObject temp = (GameObject) Instantiate(PopScore);
		TextFly text = temp.GetComponent<TextFly>();
		temp.name = transform.name+"_Text";
		
		text.Target(transform, score);

		if (addingScore)
		{
			toAdd += score;
			return;
		}
			
		StartCoroutine(IncrementScore(score));

	}
	
	public void CreatePoppingText(Transform transform, string _text)
	{
		GameObject temp = (GameObject)Instantiate(PopScore);
		TextFly text = temp.GetComponent<TextFly>();
		temp.name = transform.name + "_Text";

		text.Target(transform, _text);

	}

	public void FillPukeSprite(float time)
	{
		if (!PukeFill) return;

		StartCoroutine(FillActionSprite(PukeFill, time));
	}
	public void FillPoopSprite(float time)
	{
		if (!PoopFill) return;

		StartCoroutine(FillActionSprite(PoopFill, time));
	}

	IEnumerator FillActionSprite(UISprite _s, float _t)
	{
		float fill = _t / 1f;
		_s.fillAmount = 0f;

		_s.gameObject.SetActive(true);

		while(_s.fillAmount < 1f)
		{
			_s.fillAmount += Time.deltaTime / fill;

			yield return null;
		}

		_s.fillAmount = 0f;
		_s.gameObject.SetActive(false);

	}


	int toAdd = 0;
	bool addingScore = false;
	IEnumerator IncrementScore(int amount)
	{

		if (addingScore) yield break;

		float timeCount = 0.04f;

		toAdd += amount;
		addingScore = true;

		while (toAdd > 0)
		{
			World.Instance.UIPoints++;
			toAdd--;

			//TODO Maybe play sound here?? Very Quiet
			yield return new WaitForSeconds(timeCount -= 0.0025f);
		}

		toAdd = 0;
		addingScore = false;
	}



}
