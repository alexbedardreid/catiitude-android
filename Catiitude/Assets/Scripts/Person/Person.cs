﻿using UnityEngine;
using System.Collections;

public class Person : MonoBehaviour
{
	public Vector2 roomBounds = new Vector2();
	public float m_maxSpeed = 1.0f;
	public float m_movementSpeed = 365.0f;

	//public bool m_walking;
	//public bool m_standing;

	private Animator m_animator;
	private SpriteRenderer m_renderer;

	private Transform m_catTransform;

	protected World _World;
	protected World m_world { get { if (!_World)_World = World.Instance; return _World; } set { _World = value; } }

	protected Cat _cat;

	bool m_facingRight = false;

	float m_speed = 0.0f;

	public float m_stateChangeTime = 10.0f;

	public int m_value = 0;

	new private Transform transform;

	void Awake()
	{
		transform = gameObject.transform;

		m_world = World.Instance;
		m_animator = this.GetComponent<Animator>();
		m_renderer = this.GetComponent<SpriteRenderer>();

	}

	void Start ()
	{
		StartCoroutine("ChangeState");
	}

	void Update ()
	{
		if(!_cat)
		{
			_cat = m_world._cat;
			m_catTransform = _cat.transform;
		}

		if(m_catTransform.position.y > -0.4)
		{
			m_renderer.sortingOrder = 5;
		}
		else
		{
			m_renderer.sortingOrder = 2;
		}


		UpdatePhysics();
	}

	IEnumerator ChangeState()
	{
		for(;;)
		{
			m_value = UnityEngine.Random.Range(0,6);

			if(m_value > 13)
			{
				m_speed = -0.5f;
			}
			else if(m_value < 7)
			{
				m_speed = 0.5f;
			}
			else
			{
				m_speed = 0.0f;
			}

			yield return new WaitForSeconds(m_stateChangeTime);
		}
	}

	void UpdatePhysics()
	{
		float h = m_speed;
		
		m_animator.SetFloat("Speed",Mathf.Abs(h));
		
		if(h * this.GetComponent<Rigidbody2D>().velocity.x < m_maxSpeed)
		{
			this.GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * m_movementSpeed);
		}
		
		if(Mathf.Abs(this.GetComponent<Rigidbody2D>().velocity.x) > m_maxSpeed)
		{
			this.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(this.GetComponent<Rigidbody2D>().velocity.x) * m_maxSpeed, this.GetComponent<Rigidbody2D>().velocity.y);
		}

		if (h > 0 && !m_facingRight || transform.position.x > roomBounds.y)
		{
			if(m_speed > 0f) Flip();
		}
		else if (h < 0 && m_facingRight || transform.position.x < roomBounds.x)
		{
			if (m_speed < 0f) Flip();
		}
	}

	void Flip ()
	{
		m_facingRight = !m_facingRight;
		
		Vector3 l_scale = transform.localScale;
		l_scale.x *= -1;
		this.transform.localScale = l_scale;

		if (m_facingRight && m_speed < 0)
			m_speed *= -1f;
		else if (!m_facingRight && m_speed > 0)
			m_speed *= -1f;
		else m_speed = 0;
	}
}
