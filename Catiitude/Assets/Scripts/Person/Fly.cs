﻿using UnityEngine;
using System.Collections;

public class Fly : MonoBehaviour
{

	Vector2 direction;
	bool moving = true;

	void Start()
	{
		StartCoroutine(Move());
	}

	IEnumerator Move()
	{
		float time = 0f;
		int max;
		while (moving)
		{
			max = waitTime();
			direction = newDirection();
			GetComponent<Rigidbody2D>().AddForce(direction, ForceMode2D.Force);
			GetComponent<Rigidbody2D>().AddForce(direction, ForceMode2D.Force);
			while (time < max)
			{
				
				transform.up = GetComponent<Rigidbody2D>().velocity;

				time += Time.deltaTime;
				yield return null;
			}
			time = 0f;
		}

	}

	Vector2 newDirection()
	{
		return new Vector2(
			Random.Range(-0.25f, 0.25f),
			Random.Range(-0.25f, 0.25f));
	}

	int waitTime()
	{
		return Random.Range(1, 3);
	}

	public void Stop()
	{
		moving = false;
		StopAllCoroutines();
		this.enabled = false;
	}

}
