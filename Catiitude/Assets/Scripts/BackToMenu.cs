﻿using UnityEngine;
using System.Collections;

public class BackToMenu : MonoBehaviour
{
	void Awake ()
	{
		Invoke ("GoToCredits",5.0f);
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
			Application.LoadLevel("Menu");
	}

	void GoToCredits()
	{
		Application.LoadLevel("Menu");
	}
}
