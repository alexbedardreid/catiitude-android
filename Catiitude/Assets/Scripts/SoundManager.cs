﻿using UnityEngine;
using System.Collections;

namespace Sounds
{
	[RequireComponent (typeof (AudioSource))]
	public class SoundManager : MonoBehaviour 
	{
		#region Script Instance
		private static SoundManager instance = null;
		
		private SoundManager() { }
		
		public static SoundManager Instance
		{
			get
			{
				return instance;
			}
		}
		
		void Awake()
		{
			if (SoundManager.Instance && !(SoundManager.Instance == this))
				gameObject.SetActive(false);
			else
				instance = this;
			
		}
		
		#endregion

		[System.Serializable]
		public struct SoundsList
		{
			public AudioClip collect;
			public AudioClip Jump;
		}

		public SoundsList soundsList;

		AudioSource source;
		void Start()
		{
			if(!source) source = this.GetComponent<AudioSource>();
		}

		public static void PlaySound(SOUNDS sound)
		{
			if(!instance) return;
			instance.playSound(sound);
		}

		void playSound(SOUNDS sound)
		{
			if(!gameObject.activeInHierarchy) return;

			switch(sound)
			{
				case SOUNDS.COLCT:source.PlayOneShot(soundsList.collect); break;
				case SOUNDS.JUMP:source.PlayOneShot(soundsList.Jump); break;

				default: throw new System.ApplicationException(sound + " Not Implemented");
			}
		}

	}

	public enum SOUNDS
	{
		JUMP = 0,
		COLCT

	}
}
