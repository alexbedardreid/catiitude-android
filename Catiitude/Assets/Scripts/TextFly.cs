﻿using UnityEngine;
using System.Collections;

public class TextFly : MonoBehaviour 
{
	public UILabel pointLabel;

	Camera gameCamera;
	Camera uiCamera;
	
	Transform target;
	new Transform transform;
	
	public void Target(Transform _t, int score = 0)
	{
		pointLabel .color = Color.clear;
		target = _t;

		if(score > 0) pointLabel.text = "+" + score;
		else
		{
			pointLabel.text = "";
			Kill ();
		}
		
		this.transform = gameObject.transform;
	}
	public void Target(Transform _t, string text)
	{
		pointLabel.color = Color.clear;
		target = _t;

		if (!string.IsNullOrEmpty(text)) pointLabel.text = text;
		else
		{
			pointLabel.text = "";
			Kill();
		}

		this.transform = gameObject.transform;
	}
	
	public void Kill()
	{
		Destroy(gameObject);
	}
	
	void Awake()
	{
		pointLabel .color = Color.clear;
		gameCamera = UIManager.Instance.MainCamera;
		uiCamera = UIManager.Instance.UICamera;
	}

	bool ScaleAndKill =false;

	float lerp = 0f;
	void Update()
	{
		if (target)
		{
			pointLabel.color = Color.Lerp(Color.clear, Color.white, lerp += Time.deltaTime * 2f);
			transform.position = WorldToScreen(target.position);
			
		}
		else if (!target && gameObject.activeSelf)
			Destroy(gameObject);
		
	}
	
	Vector2 WorldToScreen(Vector3 Pos)
	{
		Vector3 tempVec = gameCamera.WorldToViewportPoint(Pos);
		
		return (uiCamera.ViewportToWorldPoint(tempVec));
	}
}
