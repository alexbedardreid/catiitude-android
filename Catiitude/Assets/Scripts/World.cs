﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

using GameManaging;

public class World : MonoBehaviour
{

	public GameObject m_poopObject;
	public GameObject m_barfObject;

	public List<Location> m_locations = new List<Location>();
	public List<BoxCollider2D> m_obstacles = new List<BoxCollider2D>();

	public bool CanContinue = false;
	public Transform m_catTransform;

	UIButton ContinueButton { get { return UIManager.Instance.ContinueButton; } }
	UILabel PointsLabel { get { return UIManager.Instance.PointsLabel; } }
	public int UIPoints = 0;

	bool _inTutorial = false;
	public static bool inTutorial { get { return instance._inTutorial; }
		private set { instance._inTutorial = value; } }

	TutorialController tutorialController;

	bool countingTime = false;
	float TotalTime = 0f;

	protected Cat _Cat;
	public Cat _cat { get { if (!_Cat)_Cat = GameObject.FindObjectOfType<Cat>(); return _Cat; } set { _Cat = value; } }

	

	public static List<Location> GetLocationsKnockLocations
	{
		get{return instance.m_locations.FindAll(x => x is KnockOver);}
	}

	public static List<Location> GetLocations(Type type)
	{
		return instance.m_locations.FindAll(x => x.GetType() == type);
	}

	bool ShouldReset = false;
	bool NormalGame = false;

	bool isSocial { get { return Social.localUser.authenticated; } }

	Level[] levels;
	int currentLevel = 0;
	public Dictionary<string, bool> actionPressed = new Dictionary<string, bool>();

	#region Script Instance
	void OnDestroy()
	{
		instance = null;
	}

	private static World instance = null;

	private World() { }

	public static World Instance
	{
		get
		{
			return instance;
		}
	}

	void Start()
	{
		if (World.Instance && !(World.Instance == this))
			gameObject.SetActive(false);
		else
			instance = this;

		Init();
	}

	#endregion

	#region Init

	void Init()
	{
		if (!_cat) _cat = Component.FindObjectOfType<Cat>();
		m_catTransform = _cat.transform;

		Location[] l_objects = Component.FindObjectsOfType<Location>();

		foreach (Location go in l_objects)
		{
			m_locations.Add(go);
		}

		GameObject[] c_objects = GameObject.FindGameObjectsWithTag("Obstacle");

		foreach (GameObject go in c_objects)
		{
			m_obstacles.Add(go.GetComponent<BoxCollider2D>());
		}

		if (tutorialController = this.GetComponent<TutorialController>())
			inTutorial = true;
		else inTutorial = false;

		SetupGameMode();

		if(ShouldReset) return;

		actionPressed.Add ("Barf",false);
		actionPressed.Add ("Poop",false);
		
		actionPressed.Add ("Attack",false);
		
		actionPressed.Add ("Eat",false);
		
		actionPressed.Add ("Bite",false);
		actionPressed.Add ("Hiss",false);
		actionPressed.Add ("Stare",false);
		actionPressed.Add ("KnockOver",false);
		actionPressed.Add ("Sit",false);
		
		actionPressed.Add ("Ignore",false);
		actionPressed.Add ("Sleep",false);
		actionPressed.Add ("Lick",false);

		levels = GameManager.Levels;

		if (PlayerPrefs.GetInt("TutorialComplete", 0) == 1 && Application.loadedLevelName == "LevelOne")
			currentLevel++;

		ShouldReset = true;
	}
	#endregion

	public void AddScore(int score)
	{
		_cat.AddScore(score);
	}

	void SetupGameMode()
	{
		if (GameManager.CurrentMode == MODE.NORMAL)
		{
			NormalGame = true;
			countingTime = true;
			TotalTime = 0f;
		}
		else // If its Random Mode
		{
			UIManager.Instance.TimeLabel.gameObject.SetActive(false);
		}
	}

	#region Update Loop

	bool inMenuScene = false;// Dont update UI stuff, if world exists within the Menu scene
	bool loading = false;
	void Update()
	{
		if (inMenuScene) return;
		if (countingTime) UIManager.time = IncrementTime();

		if (Input.GetKeyDown(KeyCode.F2))
			Debug.LogError("Points: " + _cat.m_catiitudePoints + "/" + levels[currentLevel].pointsRequired);

		//TODO Need to add a screen that doesn"t just jump to the next level
		if (Input.GetKeyDown(KeyCode.F1))
		{
			if (levels[currentLevel].NextLevel != "" 
			    && Application.loadedLevelName != levels[currentLevel].NextLevel)
			{
				if (Application.loadedLevelName == "Tutorial")
				{
					PlayerPrefs.SetInt("DoneTut", 1);
					PlayerPrefs.Save();
				}

				_cat.m_catiitudePoints = 0;
				loading = true;
				Application.LoadLevel(levels[currentLevel].NextLevel);
			}
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.LoadLevel("Credits");
		}

		if(NormalGame)
		{
			if (_cat.m_catiitudePoints >= levels[currentLevel].pointsRequired)
			{
				//Set bool true to display the Next Button
				_cat.Emit(1);
				CanContinue = true;
				countingTime = false;

			}
		}
		else
		{
			if (TotalTime <= 0f && countingTime)
			{
				CanContinue = true;
				countingTime = false;
			}
			else if (_cat.m_catiitudePoints >= levels[currentLevel].pointsRequired)
			{
				//Set bool true to display the Next Button
				_cat.Emit(1);
				CanContinue = true;
				countingTime = false;

			}
		}

		
			

		if(loading)
		{
			if(Application.loadedLevelName == levels[currentLevel].NextLevel)
			{
				currentLevel++;
				loading = false;
				CanContinue = false;

				//Move the Cat to a start location
				GameObject startPos;
				if (startPos = GameObject.Find("CAT_START"))
					m_catTransform.position = startPos.transform.position;

				m_obstacles.Clear();
				m_locations.Clear();

				Init();
			}
		}
	}

	float timeCheck;
	float IncrementTime()
	{
		if (GameManager.CurrentMode == MODE.NORMAL)
			return (float)Math.Round((TotalTime += Time.deltaTime), 2);

		return 0f;
	}

	void LateUpdate()
	{
		if (inMenuScene) return;

		ContinueButton.gameObject.SetActive(CanContinue);

		PointsLabel.text = "Catiitude Points: " + UIPoints + "/" + levels[currentLevel].pointsRequired;
	}
	#endregion //Update Loop

	public void ActionNotification(string l_name)
	{
		foreach (Location loc in m_locations)
		{
			loc.ActionNotification(l_name);
		}
	}

	#region On UI Pressed
	public void LoadNextLevelPressed()
	{
		if (_cat.m_catiitudePoints >= levels[currentLevel].pointsRequired)
		{
			switch (GameManager.CurrentMode)
			{
				case MODE.NORMAL:
					LoadNextLevel();
					break;
				case MODE.HARDCORE:
				case MODE.RANDOM:
					LoadMenu();
					break;
				default:
					throw new NotImplementedException(GameManager.CurrentMode + " is not implemented");
			}
						
		}
	}

	void LoadMenu()
	{
		Application.LoadLevel("Menu");
		//TODO Open levels menu, select level

		inMenuScene = true;

		StartCoroutine(loadMenu());

		DontDestroy[] _ds = Component.FindObjectsOfType<DontDestroy>();

		foreach (DontDestroy _d in _ds)
		{
			if (_d.gameObject == this.gameObject) continue;

			Destroy(_d.gameObject);
		}

	}

	IEnumerator loadMenu()
	{
		while(Application.loadedLevelName != "Menu")
		{
			yield return null;
		}


		Menu _mnu = Component.FindObjectOfType<Menu>();
		if (_mnu)
		{
			_mnu.OnLevelsPressed();

			switch (currentLevel)
			{
				case 1:
					_mnu.OnLevelOnePressed();
					break;
				case 2:
					_mnu.OnLevelTwoPressed();
					break;
				default: break;
			}
		}
		else Debug.LogError("No Menu Found");

		Destroy(this.gameObject);
	}

	void LoadNextLevel()
	{
		if (!checkCanLoadLevel(currentLevel)) return;


		#region Check Level Completion
		if (Application.loadedLevelName == levels[0].levelSceneName)
		{
			UnlockAchievement(GameManager.ACH_CAT_KNOWLEDGE);// Tutorial Completed

			SaveLevelStats(levels[0].levelSceneName);
		}
		else if (Application.loadedLevelName == levels[1].levelSceneName)
		{
			UnlockAchievement(GameManager.ACH_LIVING_ROOM_CONQUERER);// LevelOne Completed
			IncrementAchievement(GameManager.ACH_GROUND_CAT_DAY, 1);

			SaveLevelStats(levels[1].levelSceneName);

		}
		else if (Application.loadedLevelName == levels[2].levelSceneName)
		{
			UnlockAchievement(GameManager.ACH_DESTROYER_OF_BEDROOMS);// LevelTwo Completed

			SaveLevelStats(levels[2].levelSceneName);
		}
		#endregion

		Poop poo = (Poop)_cat.GetAction("Poop");
		poo.Full = true;

		Barf barf = (Barf)_cat.GetAction("Barf");
		barf.HairBall = true;

		CanContinue = false;
		_cat.m_catiitudePoints = 0;
		UIPoints = 0;
		loading = true;

		Application.LoadLevel(levels[currentLevel].NextLevel);
	}

	bool checkCanLoadLevel(int id)
	{
		if (GameManager.CurrentMode != MODE.NORMAL) return false;
		if (levels[id].levelID < 0) return false;
		if (string.IsNullOrEmpty(levels[id].NextLevel)) return false;
		if (Application.loadedLevelName == levels[id].NextLevel) return false;

		return true;
	}

	/// <summary>
	/// Saves and updates level information to be viewed on the levels UI screen.
	/// </summary>
	/// <param name="levelName"></param>
	void SaveLevelStats(string levelName)
	{
		//TODO Add the GameType name here
		if (PlayerPrefs.GetInt(levelName + "Complete", 0) == 0)
		{
			PlayerPrefs.SetInt(levelName + "Complete", 1);
			PlayerPrefs.SetInt(levelName + "Points", _cat.m_catiitudePoints);

			if (TotalTime > 0f) PlayerPrefs.SetFloat(levelName + "Time", TotalTime);
			
		}
		else
		{
			if (PlayerPrefs.GetInt(levelName + "Points", 0) > _cat.m_catiitudePoints)
				PlayerPrefs.SetInt(levelName + "Points", _cat.m_catiitudePoints);

			if (TotalTime > 0f)
			{
				if (PlayerPrefs.GetFloat(levelName + "Time", 999f) > TotalTime)
					PlayerPrefs.SetFloat(levelName + "Time", TotalTime);
			}
		}

		PlayerPrefs.Save();

		//TODO Only save if its normal to the leaderboard for now
		GameManager.PublishtoLeaderboard(GameManager.GetLeaderboardCode(levels[currentLevel].levelID),
				TotalTime * 1000f);
	}

	#region UIAction Press
	void PerformAction(string _action)
	{
		actionPressed[_action] = true;
		_Cat.CheckAllActions = true;

		if (_inTutorial) tutorialController.SetActionActive(_action);
	}

	public void OnPoopPressed()
	{
		PerformAction("Poop");
	}
	public void OnPukePressed()
	{
		PerformAction("Barf");
	}
	public void OnHitPressed()
	{
		PerformAction("Attack");
	}
	public void OnSitPressed()
	{
		PerformAction("Sit");
	}
	public void OnSleepPressed()
	{
		PerformAction("Sleep");
	}
	public void OnEatPressed()
	{
		PerformAction("Eat");
	}
	public void OnLickPressed()
	{
		PerformAction("Lick");
	}
	#endregion

	#endregion //On UI Pressed

	#region Achievments

	public static void ShowAchievement(string id)
	{ GameManager.ShowAchievement(id); }

	public static void UnlockAchievement(string id)
	{ GameManager.UnlockAchievement(id); }

	public static void IncrementAchievement(string id, int amount)
	{ GameManager.IncrementAchievement(id, amount); }

	#endregion

	#region Obstacles
	public void EnableObstacles()
	{
		foreach (BoxCollider2D obs in m_obstacles)
		{
			if (!obs) continue;

			obs.enabled = true;
		}
	}

	public void CheckObstacles(Vector2 _pos)
	{
		//Debug.LogError("Check");
		foreach (BoxCollider2D obs in m_obstacles)
		{
			if (!obs) continue;
			
			obs.enabled = (obs.transform.position.y < _pos.y);
		}
	}

	public void DisableObstaclesAbove()
	{
		foreach (BoxCollider2D obs in m_obstacles)
		{
			if (!obs) continue;

			if (!m_catTransform) Debug.Log("m_catTransform");

			if (obs.transform.position.y > m_catTransform.position.y)
			{
				obs.enabled = false;
			}
		}
	}

	public void DisableObstaclesBelow()
	{
		foreach (BoxCollider2D obs in m_obstacles)
		{
			if (obs.transform.position.y < m_catTransform.position.y)
			{
				obs.enabled = false;
			}
		}
	}
	#endregion
}

/*
class Level
{
	public string LevelName {get; private set;}
	public string NextLevel{get; private set;}
	public int LevelMaxPoints{get; private set;}

	public Level()
	{
		LevelName = "";
		NextLevel = "";
		LevelMaxPoints = 0;
	}
	public Level(string levelName, string NextLevel, int needPoints)
	{
		LevelName = levelName;
		NextLevel = NextLevel;
		LevelMaxPoints = needPoints;
	}


}*/





