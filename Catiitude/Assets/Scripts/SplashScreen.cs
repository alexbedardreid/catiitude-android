﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour
{
	void Start ()
	{
		Invoke("ChangeScene",3.0f);
	}

	void ChangeScene()
	{
		Application.LoadLevel("Menu");
	}
}
