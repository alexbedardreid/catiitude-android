﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour
{
	public float m_lifeTime = 1.0f;
	public float m_currentLifeTime = 0.0f;

	void Start()
	{
		Invoke ("DisableCollision",0.5f);
	}
	
	void Update ()
	{
		m_currentLifeTime += Time.deltaTime;

		if(m_currentLifeTime >= m_lifeTime)
		{
			m_currentLifeTime = 0.0f;
			Destroy(this.gameObject);
		}
	}

	void DisableCollision()
	{
		this.transform.GetComponent<Rigidbody2D>().isKinematic = true;
	}
}
