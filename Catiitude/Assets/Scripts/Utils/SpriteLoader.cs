﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public static class SpriteLoader 
{

	static Dictionary<int, Sprite> collection = new Dictionary<int, Sprite>();

	public static Sprite GetSprite(int type)
	{
		Sprite sprite;
		int id = (type) * 11;

		if (collection.TryGetValue(id, out sprite)) return sprite;
		else { return LoadSprite((LocationCreator.LocationType)type); }
	}
	public static Sprite GetSprite(LocationCreator.LocationType type)
	{
		Sprite sprite;
		int id = ((int)type) * 11;

		if (collection.TryGetValue(id, out sprite)) return sprite;
		else { return LoadSprite(type); }
	}


	static Sprite LoadSprite(LocationCreator.LocationType type)
	{
		int id = ((int)type) * 11;

		string path = "Catii_Sheet";
		string spriteName = "";

		switch (type)
		{
			case LocationCreator.LocationType.Barf:
				spriteName = "Puke"; break;
			case LocationCreator.LocationType.Eat:
				return null;
			case LocationCreator.LocationType.Knock:
				spriteName = "Hit"; break;
			case LocationCreator.LocationType.Poop:
				spriteName = "poop"; break;
			case LocationCreator.LocationType.Sit:
				spriteName = "Sit"; break;
			case LocationCreator.LocationType.Sleep:
				spriteName = "Sleep"; break;
		}
		spriteName += "_ICO";

		Sprite[] loaded = Resources.LoadAll<Sprite>(path);
		Sprite sprite = loaded.FirstOrDefault(x => x.name == spriteName);
		loaded = null;

		if(sprite)
		{
			collection.Add(id, sprite);
			return sprite;
		}

		return null;
	}

}
