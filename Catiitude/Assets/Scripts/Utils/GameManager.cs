﻿using UnityEngine;
using System.Collections;

using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;

namespace GameManaging
{
	public enum MODE
	{
		NORMAL,		// Complete level, track time
		HARDCORE,	// Still needs some work?
		TIME_TRIAL,	// Complete Level under certain Time
		RANDOM		// Randomize but don't keep Leaderboard score, COmplete All
	}

	public enum ACTION : int
	{
		BARF = 2063059,
		POOP = 2493504,
		ATTACK = 1971575400,
		EAT = 69432,
		BITE = 2070808,
		HISS = 2249537,
		STARE = 80204851,
		SIT = 83134,
		IGNORE = -2106529294,
		SLEEP = 79969975,
		LICK = 2368197
	}

	public static class GameManager
	{
		static bool isSocial { get { return Social.localUser.authenticated; } }

		public static MODE CurrentMode = MODE.NORMAL;

		public static readonly Level[] Levels = 
		{
			//Level(ID, SceneName, UI Name, NextLevel Scene Name,Points, TimeGoals)
			new Level(0, "Tutorial", "Tutorial","LevelOne", 15),
			new Level(1, "LevelOne", "Living Room","LevelTwo", 20, new int[3] {13,25,35}),
			new Level(2, "LevelTwo", "Bedroom", "Credits", 30, new int[3] {20,30,45})
		};

		#region Achievementents

		#region Acheivement Codes

		public const string ACH_CAT_KNOWLEDGE = "CggImp3fgFAQAhAG";
		public const string ACH_LITTLE_POUNCER = "CggImp3fgFAQAhAB";
		public const string ACH_LIVING_ROOM_CONQUERER = "CggImp3fgFAQAhAC";
		public const string ACH_DESTROYER_OF_BEDROOMS = "CggImp3fgFAQAhAD";
		public const string ACH_PURE_ANARCHY = "CggImp3fgFAQAhAE";
		public const string ACH_LITTLE_COMFORTS = "CggImp3fgFAQAhAF";
		public const string ACH_POOP_MASTER = "CggImp3fgFAQAhAH";
		public const string ACH_FURGATORY = "CggImp3fgFAQAhAI";
		public const string ACH_GROUND_CAT_DAY = "CggImp3fgFAQAhAJ";
		public const string ACH_YOURE_AN_ADULT = "CggImp3fgFAQAhAK";
		public const string ACH_A_COMFORTABLE_BUTT = "CggImp3fgFAQAhAL";
		public const string ACH_THE_GRAND_MASTER_OF_HOOMANS = "CggImp3fgFAQAhAM";
		public const string ACH_WHEN_CATS_FLY = "CggImp3fgFAQAhAO";

		#endregion



		public static void ShowAchievement(string id)
		{
			if (!isSocial) return;

			PlayGamesPlatform.Instance.ReportProgress(
				id, 0f, (bool success) =>
				{
					// handle success or failure
				});

		}

		public static void UnlockAchievement(string id)
		{
			if (!isSocial) return;

			PlayGamesPlatform.Instance.ReportProgress(
				id, 100f, (bool success) =>
				{
					// handle success or failure
				});
		}

		public static void IncrementAchievement(string id, int amount)
		{
			if (!isSocial) return;

			PlayGamesPlatform.Instance.IncrementAchievement(
				id, amount, (bool success) =>
				{
					// handle success or failure
				});
		}
		#endregion // Achiemevents

		#region Leaderboards

		public const string LEAD_QUICKEST_TUTORIAL = "CggImp3fgFAQAhAQ";
		public const string LEAD_QUICKEST_LIVING_ROOM = "CggImp3fgFAQAhAP";
		public const string LEAD_QUICKEST_BEDROOM = "CggImp3fgFAQAhAR";

		public static string GetLeaderboardCode(int levelid)
		{
			if (!isSocial) return "";

			switch(levelid)
			{
				case 0: return LEAD_QUICKEST_TUTORIAL;
				case 1: return LEAD_QUICKEST_LIVING_ROOM;
				case 2: return LEAD_QUICKEST_BEDROOM;
				default: return "";
			}

		}

		public static void PublishtoLeaderboard(string id, float amount)
		{
			if (!isSocial) return;

			PlayGamesPlatform.Instance.ReportScore(
				(long)amount, id, (bool success) =>
				{
					// handle success or failure
				});
		}

		#endregion






	}

	public class Level
	{
		public string levelSceneName;
		public string UIDisplayName;
		public string NextLevel;
		public int levelID;
		public int pointsRequired;
		public int[] TimeGoals;



		public Level()
		{
			levelID = -1;
			levelSceneName = "";
			UIDisplayName = "";
			NextLevel = "";
			pointsRequired = -1;

			TimeGoals = new int[] { -1, -1, -1 };
		}

		public Level(int id, string scene, string uiName, string next, int points)
		{
			levelID = id;
			levelSceneName = scene;
			UIDisplayName = uiName;
			NextLevel = next;
			pointsRequired = points;

			TimeGoals = new int[] { 0, 0, 0 };
		}

		public Level(int id, string scene, string uiName, string next, int points, int[] times)
		{
			levelID = id;
			levelSceneName = scene;
			UIDisplayName = uiName;
			NextLevel = next;
			pointsRequired = points;

			TimeGoals = times;
		}



	}
}
