﻿using UnityEngine;
using System.Collections;

public class DontDestroy : MonoBehaviour 
{
	public bool DestroyAtCredits;

	// Use this for initialization
	void Start () 
	{
		DontDestroyOnLoad(this.gameObject);
	}

	void Update()
	{
		if(DestroyAtCredits)
		{
			if(Application.loadedLevelName == "Credits")
			{Destroy(this.gameObject);}

		}

	}
}
