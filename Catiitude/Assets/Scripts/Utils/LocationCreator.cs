﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using System;
using System.Reflection;
using UnityEditor;

[CustomEditor(typeof(LocationCreator))]
public class LocationCreatorEditor : Editor
{
	Transform trans;
	float size = 0.5f;
	bool test = false;

	public override void OnInspectorGUI()
	{
		LocationCreator location;
		if (!(location = target as LocationCreator)) return;
		trans = location.gameObject.transform;

		if (location.enabled)
		{
			GUILayout.BeginHorizontal();
			location.type = (LocationCreator.LocationType)EditorGUILayout.EnumPopup("Location Type: "
				, location.type);
			trans.gameObject.name = location.type + " Location";
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			location.PointAwarded = EditorGUILayout.IntField("Points Awarded: ", location.PointAwarded);
			location.runOnce = EditorGUILayout.Toggle("Run Once: ", location.runOnce);
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			location.showIcon = EditorGUILayout.Toggle("Show Icon: ", location.showIcon);
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Collider Size: ");
			location.ColliderSize = EditorGUILayout.Slider(location.ColliderSize, 0.1f, 2f);
			size = location.ColliderSize / 2f;
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			location.special = EditorGUILayout.Toggle("Special Completion: ", location.special);
			GUILayout.EndHorizontal();

			if(location.type == LocationCreator.LocationType.Knock)
			{
				GUILayout.BeginHorizontal();
				location.force = EditorGUILayout.Vector2Field("Force: ", location.force);
				GUILayout.EndHorizontal();

				GUILayout.BeginHorizontal();
				location.torque = EditorGUILayout.FloatField("Torque: ", location.torque);
				GUILayout.EndHorizontal();
			}
		}
		else
		{
			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Location Type:  ");
			EditorGUILayout.LabelField(location.type.ToString());
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Points Awarded: ");
			EditorGUILayout.LabelField(location.PointAwarded.ToString());
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Run Once: ");
			EditorGUILayout.LabelField(location.runOnce.ToString());
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Show Icon: ");
			EditorGUILayout.LabelField(location.showIcon.ToString());
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Collider Size: ");
			EditorGUILayout.LabelField(location.ColliderSize.ToString());
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel("Special Completion: ");
			EditorGUILayout.LabelField(location.special.ToString());
			GUILayout.EndHorizontal();

			if (location.type == LocationCreator.LocationType.Knock)
			{
				GUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Force: ");
				EditorGUILayout.LabelField(location.force.ToString());
				GUILayout.EndHorizontal();

				GUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Torque: ");
				EditorGUILayout.LabelField(location.torque.ToString());
				GUILayout.EndHorizontal();
			}
		}



	}

	void OnSceneGUI()
	{
		if(!trans) return;
		LocationCreator location;

		if (!(location = target as LocationCreator)) return;
		trans = location.gameObject.transform;

		Vector3 topLeft = new Vector3(trans.position.x - size, trans.position.y - size, 0.1f);
		Vector3 topRight = new Vector3(trans.position.x + size, trans.position.y - size, 0.1f);

		Vector3 botLeft = new Vector3(trans.position.x - size, trans.position.y + size, 0.1f);
		Vector3 botRight = new Vector3(trans.position.x + size, trans.position.y + size, 0.1f);

		if(location.enabled)
			IconManager.DrawIcon(trans.gameObject, (int)IconManager.LabelIcon.Blue);
		else
			IconManager.DrawIcon(trans.gameObject, (int)IconManager.LabelIcon.Green);

		Handles.color = Color.green;
		Handles.DrawLine(topLeft, topRight);
		Handles.DrawLine(topRight, botRight);
		Handles.DrawLine(botRight, botLeft);
		Handles.DrawLine(botLeft, topLeft);
		

		if (GUI.changed) EditorUtility.SetDirty(target);
	}

	[MenuItem("GameObject/Create Location")]
	public static void CreateLocation()
	{
		GameObject obj = new GameObject();
		obj.transform.position = Vector2.zero;

		obj.AddComponent<LocationCreator>();
		obj.name = "Location";
	}
}

#region Icon Manager
public class IconManager
{

	public enum LabelIcon
	{
		Gray = 0,
		Blue,
		Teal,
		Green,
		Yellow,
		Orange,
		Red,
		Purple
	}

	public enum Icon
	{
		CircleGray = 0,
		CircleBlue,
		CircleTeal,
		CircleGreen,
		CircleYellow,
		CircleOrange,
		CircleRed,
		CirclePurple,
		DiamondGray,
		DiamondBlue,
		DiamondTeal,
		DiamondGreen,
		DiamondYellow,
		DiamondOrange,
		DiamondRed,
		DiamondPurple
	}

	private static GUIContent[] labelIcons;
	private static GUIContent[] largeIcons;

	public static void DrawIcon(GameObject gameObject, int idx)
	{
		var largeIcons = GetTextures("sv_label_", string.Empty, 0, 8);
		var icon = largeIcons[idx];
		var egu = typeof(EditorGUIUtility);
		var flags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
		var args = new object[] { gameObject, icon.image };
		var setIcon = egu.GetMethod("SetIconForObject", flags, null, new Type[] { typeof(UnityEngine.Object), typeof(Texture2D) }, null);
		setIcon.Invoke(null, args);
	}

	public static GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count)
	{
		GUIContent[] array = new GUIContent[count];
		for (int i = 0; i < count; i++)
		{
			array[i] = EditorGUIUtility.IconContent(baseName + (startIndex + i) + postFix);
		}
		return array;
	}
}
#endregion

#endif

public class LocationCreator : MonoBehaviour 
{
	public enum LocationType
	{
		Eat = 0,
		Knock,
		Barf,
		Poop,
		Sit,
		Sleep,
		Random
	};

	public LocationType type;
	public bool runOnce = false;
	public bool showIcon = true;
	public bool special = false;
	public int PointAwarded = 0;
	public float ColliderSize = 1f;
	Location thisLocation;
	BoxCollider2D collider;

	public Vector2 force;
	public float torque;

	void Start()
	{
		if (GameManaging.GameManager.CurrentMode == GameManaging.MODE.RANDOM
			&& type != LocationType.Eat
			&& type != LocationType.Knock)//TODO Setup the knock over random
			type = LocationType.Random;


		gameObject.layer = LayerMask.NameToLayer("Locations");
		gameObject.tag = "Locations";

		collider = gameObject.AddComponent<BoxCollider2D>();
		collider.isTrigger = true;
		collider.size = new Vector2(ColliderSize, ColliderSize);

		GameObject icon = new GameObject("Icon");
		icon.transform.parent = transform;
		icon.transform.localPosition = new Vector2(0f, 0.4f);
		icon.transform.localScale = new Vector2(2f, 2f);

		SpriteRenderer sprite =  icon.AddComponent<SpriteRenderer>();
		sprite.color = Color.clear;
		sprite.sortingLayerID = 10;

		if (type == LocationType.Random)
		{
			type = (LocationType)UnityEngine.Random.Range(2, 6);
			if (PointAwarded == 0) PointAwarded = UnityEngine.Random.Range(1, 10);
			runOnce = true;
			gameObject.name = type + " Random_Location";
		}


		sprite.sprite = (showIcon ? SpriteLoader.GetSprite(type) : null);


		switch (type)
		{
			case LocationType.Barf:
				thisLocation =gameObject.AddComponent<BarfLocation>(); break;
			case LocationType.Eat:
				thisLocation =gameObject.AddComponent<EatLocation>(); break;
			case LocationType.Knock:
				thisLocation =gameObject.AddComponent<KnockOver>(); 
				((KnockOver)thisLocation).m_objectToKnockOver = transform.parent.GetComponent<Rigidbody2D>();
				((KnockOver)thisLocation).m_force = force;
				((KnockOver)thisLocation).m_torque = torque;
				icon.transform.localPosition = new Vector2(0f, 0.7f);
				break;
			case LocationType.Poop:
				thisLocation =gameObject.AddComponent<PoopLocation>(); break;
			case LocationType.Sit:
				thisLocation =gameObject.AddComponent<SitLocation>(); break;
			case LocationType.Sleep:
				thisLocation =gameObject.AddComponent<SleepLocation>(); break;
		}

		thisLocation.m_pointsAwarded = PointAwarded;
		thisLocation.m_runOnce = runOnce;
		thisLocation.m_special = special;

		//TODO Add Sprite renderer Child, with correct sprite, layer 10, and fully transparent

		this.enabled = false;
	}
}
