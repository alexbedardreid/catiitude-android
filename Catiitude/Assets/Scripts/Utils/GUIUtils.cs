﻿using UnityEngine;
using System.Collections;

public class GUIUtils
{
	public static void DrawText(string l_text, float l_x, float l_y, GUIStyle l_style)
	{
		Vector2 l_size = l_style.CalcSize(new GUIContent(l_text));

		GUI.Label(new Rect(l_x, l_y, l_size.x,l_size.y),l_text,l_style);
	}
	
	public static void DrawText(string l_text, float l_x, float l_y, GUIStyle l_style, bool l_center)
	{
		Vector2 l_size = l_style.CalcSize(new GUIContent(l_text));

		if(l_center)
		{
			l_x = l_x - (l_size.x / 2);
			l_y = l_y - (l_size.y / 2);
		}

		GUI.Label(new Rect(l_x, l_y, l_size.x,l_size.y),l_text,l_style);
	}

	public static void DrawText(string l_text, float l_x, float l_y, GUIStyle l_style, bool l_center, bool l_outline)
	{
		Vector2 l_size = l_style.CalcSize(new GUIContent(l_text));
		
		if(l_center)
		{
			l_x = l_x - (l_size.x / 2);
			l_y = l_y - (l_size.y / 2);
		}
		
		Color l_old = l_style.normal.textColor; 
		if(l_outline)
		{
			l_style.normal.textColor = Color.black;
			GUI.Label(new Rect(l_x + 1, l_y, l_size.x,l_size.y),l_text,l_style);
			GUI.Label(new Rect(l_x - 1, l_y, l_size.x,l_size.y),l_text,l_style);
			GUI.Label(new Rect(l_x, l_y + 1, l_size.x,l_size.y),l_text,l_style);
			GUI.Label(new Rect(l_x, l_y - 1, l_size.x,l_size.y),l_text,l_style);
			l_style.normal.textColor = l_old;
		}
		
		GUI.Label(new Rect(l_x, l_y, l_size.x,l_size.y),l_text,l_style);
	}
	
	public static void DrawText(string l_text, float l_x, float l_y, GUIStyle l_style, bool l_center, bool l_outline, int outLineSize, Color l_outlineColor)
	{
		Vector2 l_size = l_style.CalcSize(new GUIContent(l_text));
		
		if(l_center)
		{
			l_x = l_x - (l_size.x / 2);
			l_y = l_y - (l_size.y / 2);
		}
		
		Color l_old = l_style.normal.textColor; 
		if(l_outline)
		{
			l_style.normal.textColor = l_outlineColor;
			GUI.Label(new Rect(l_x + outLineSize, l_y, l_size.x,l_size.y),l_text,l_style);
			GUI.Label(new Rect(l_x - outLineSize, l_y, l_size.x,l_size.y),l_text,l_style);
			GUI.Label(new Rect(l_x, l_y + outLineSize, l_size.x,l_size.y),l_text,l_style);
			GUI.Label(new Rect(l_x, l_y - outLineSize, l_size.x,l_size.y),l_text,l_style);
			l_style.normal.textColor = l_old;
		}
		
		GUI.Label(new Rect(l_x, l_y, l_size.x,l_size.y),l_text,l_style);
	}

	public static void DrawButton(float x, float y, float dx, float dy, float l_padding, GUIStyle l_style, bool l_center, string l_scene)
	{
		Vector2 l_size = new Vector2(dx,dy);
		
		if(l_center)
		{
			x = x - (l_size.x / 2);
			y = y - (l_size.y / 2);
		}
		
		if(GUI.Button(new Rect(x, y, l_size.x + l_padding, l_size.y + l_padding),"",l_style))
		{
			Application.LoadLevel(l_scene);
		}
	}

	public static void DrawButton(string l_text, float x, float y, float l_padding, GUIStyle l_style, bool l_center, string l_scene)
	{
		Vector2 l_size = l_style.CalcSize(new GUIContent(l_text));
		
		if(l_center)
		{
			x = x - (l_size.x / 2);
			y = y - (l_size.y / 2);
		}
		
		if(GUI.Button(new Rect(x, y, l_size.x + l_padding, l_size.y + l_padding),l_text,l_style))
		{
			Application.LoadLevel(l_scene);
		}
	}
}
