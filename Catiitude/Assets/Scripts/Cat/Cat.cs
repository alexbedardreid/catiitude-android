﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using Sounds;
using GameManaging;

public class Cat : MonoBehaviour
{
	public bool prototype = false;

	public int m_catiitudePoints = 0;


	public float m_maxSpeed = 1.0f;

	public ParticleSystem particles;

#if UNITY_EDITOR
	public float m_movementSpeed = 30.0f;
	public float m_jumpForce = 30.0f;
#elif UNITY_ANDROID
	float m_jumpForce = 6.0f;
	float m_movementSpeed = 45.0f;
#endif

	public float input_Horizontal = 0f;
	public float input_Vertical = 0f;

	public bool CheckAllActions = false;
    public bool m_facingRight { get; private set; }

	public bool m_grounded;
	private bool m_jumping;

	private World m_world;
	private Animator m_animator;

	private Transform m_groundCheck;

	public GameObject m_actionBar;
	Transform m_actionBar_Trans;

	public BoxCollider2D m_collider;

	public Dictionary<int,Action> m_actions = new Dictionary<int, Action>();
	List<Action> m_actionsValues;

	new private Transform transform;
	new private Rigidbody2D rigidbody2D;

	

	int[] animationID = new int[]
	{
		Animator.StringToHash("Speed"),
		Animator.StringToHash("Jump"),
		Animator.StringToHash("Grounded")
	};

	public void FloatText(string _text)
	{
		UIManager.Instance.CreatePoppingText(transform, _text);
	}
	public void AddScore(int score)
	{
		//Add the flying text label here
		UIManager.Instance.CreatePoppingScore(transform, score);
		particles.Emit(5);
	}

	

	public void Emit(int amount)
	{
		particles.Emit(amount);
	}

	void Start()
	{
		rigidbody2D = gameObject.GetComponent<Rigidbody2D>();

		#if UNITY_EDITOR
		this.rigidbody2D.mass = 5f;
		#elif UNITY_ANDROID
		this.rigidbody2D.mass = 1f;
		#endif

        m_facingRight = true;

		transform = gameObject.transform;

		m_world = World.Instance;
		m_animator = this.GetComponent<Animator>();

		m_collider = this.GetComponent<BoxCollider2D>();

		m_groundCheck = transform.GetChild(3);//Grounded

		Init ();
	}

	void Init()
	{
		if(!World.Instance) Debug.LogError("1");
		if(!transform) Debug.LogError("2");
		
		m_world.
			CheckObstacles(transform.position);
		
		//Only try to find it if it doesn't exist
		//if(!m_actionBar) m_actionBar = GameObject.Find("ActionBar");
		
		//m_actionBar_Trans = m_actionBar.transform;


		m_actions.Add((int)ACTION.BARF, new Barf("Barf", transform, 3.0f));
		m_actions.Add((int)ACTION.POOP, new Poop("Poop", transform, 1.0f));

		m_actions.Add((int)ACTION.ATTACK, new Action("Attack", 0.5f));

		m_actions.Add((int)ACTION.EAT, new Eat("Eat", 1.0f));

		m_actions.Add((int)ACTION.BITE, new Action("Bite", 1.0f));
		m_actions.Add((int)ACTION.HISS, new Action("Hiss", 1.0f));
		m_actions.Add((int)ACTION.STARE, new Action("Stare", 1.0f));
		m_actions.Add((int)ACTION.SIT, new Action("Sit", 1.0f));

		m_actions.Add((int)ACTION.IGNORE, new Action("Ignore", 1.0f));
		m_actions.Add((int)ACTION.SLEEP, new Action("Sleep", 1.0f));
		m_actions.Add((int)ACTION.LICK, new Lick("Lick", 1.0f));
		
		m_actionsValues = (List<Action>)m_actions.Values.ToList();

	}

	#region Prototype
	bool spinCheck = false;
	float waitTime = 0f;
	float lerp=0f;
	float hold=0f;
	#endregion

	void Update ()
	{
# if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.F9))
			AddScore(1);
#endif
		//Ground layer is 9
		m_grounded = Physics2D.Linecast(transform.position, m_groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));


		#region Prototype
		if (prototype)
		{
			if (spinCheck)
			{
				if(waitTime >= 1.5f)
				{

					hold = Mathf.LerpAngle(transform.eulerAngles.z, 0f, lerp += Time.deltaTime);
					transform.localEulerAngles = new Vector3(0f, 0f, hold);

					if(lerp >= 1f || hold<0.0001f)
					{
						rigidbody2D.fixedAngle = true;
						spinCheck = false;
						waitTime = 0; lerp = 0;

					}
				}
				else waitTime += Time.deltaTime;
			}
			else
			{
				rigidbody2D.fixedAngle = (input_Horizontal != 0f || m_grounded);
				if (Vector2.Angle(transform.up, Vector2.up) > 0f) spinCheck = true;
			}
		}
		#endregion




#if UNITY_EDITOR
		input_Vertical = Input.GetAxis("Jump");
		//Debug.LogError(string.Format("Grounded: {0}, velocity: {1}, Jumping Value: {2}", m_grounded, Math.Round(rigidbody2D.velocity.y,3), input_Vertical));
#elif UNITY_ANDROID
		input_Vertical = DeviceInput.Vertical;
		#endif


		if (input_Vertical > 0.0f && m_grounded && Math.Round(rigidbody2D.velocity.y, 3) == 0f)
		{
			m_jumping = true;

			if (PlayerPrefs.GetInt("LittlePouncer", 0) == 0)
			{
				World.UnlockAchievement(GameManager.ACH_LITTLE_POUNCER);// Little Pouncer
				PlayerPrefs.SetInt("LittlePouncer", 1);
				PlayerPrefs.Save();
			}
			else
				World.IncrementAchievement(GameManager.ACH_WHEN_CATS_FLY, 1);//When Cats Fly

		}

		if(input_Vertical < 0.0f && m_grounded)
		{
			m_world.DisableObstaclesBelow();
			//Invoke ("EnableObstacles",0.6f);
		}

		foreach(Action l_action in m_actionsValues)
		{
			if(!l_action.m_enabled || CheckAllActions)l_action.Check();
		}

		if(CheckAllActions) CheckAllActions = false;

		//Grounded
		m_animator.SetBool(animationID[2],m_grounded);

		//UpdateActionBar();
		UpdatePhysics();
	}


	public void MoveRight()
	{
		Debug.Log("Right Pressed");
	}
	public void MoveLeft()
	{
		Debug.Log("Left Pressed");
	}

	
	void UpdatePhysics()
	{
		#if UNITY_EDITOR
		input_Horizontal = Input.GetAxis("Move");
		#elif UNITY_ANDROID
		input_Horizontal = DeviceInput.Horizontal;
		#endif

		//Speed
		m_animator.SetFloat(animationID[0],Mathf.Abs(input_Horizontal));

		if(input_Horizontal * this.rigidbody2D.velocity.x < m_maxSpeed)
		{
#if UNITY_EDITOR
			this.rigidbody2D.AddForce(Vector2.right * input_Horizontal * m_movementSpeed);
#elif UNITY_ANDROID
			this.rigidbody2D.AddForce(Vector2.right * input_Horizontal * m_movementSpeed, ForceMode2D.Force);
#endif
		}

		if(Mathf.Abs(this.rigidbody2D.velocity.x) > m_maxSpeed)
		{
			this.rigidbody2D.velocity = new Vector2(Mathf.Sign(this.rigidbody2D.velocity.x) * m_maxSpeed,
			                                        this.rigidbody2D.velocity.y);
		}

		if(input_Horizontal > 0 && !m_facingRight)
		{
			Flip();
		}
		else if(input_Horizontal < 0 && m_facingRight)
		{
			Flip();
		}

		foreach(Action l_action in m_actionsValues)
		{
			l_action.Run(m_animator);
		}

		if(m_jumping)
		{
			SoundManager.PlaySound(SOUNDS.JUMP);
			#if UNITY_EDITOR
			this.rigidbody2D.AddForce(Vector2.up * m_jumpForce, ForceMode2D.Impulse);
			#elif UNITY_ANDROID
			this.rigidbody2D.AddForce(Vector2.up * m_jumpForce, ForceMode2D.Impulse);
			#endif
			m_jumping = false;
		}
		if(rigidbody2D.velocity.y != 0f && !m_grounded)
		{
			//Jump
			m_animator.SetTrigger(animationID[1]);

			if(input_Vertical >= 0.0f)
				m_world.CheckObstacles(transform.position);
		}
	}

	public Action GetAction(ACTION _actn)
	{
		int hash = (int)_actn;
		Action action;

		if (m_actions.TryGetValue(hash, out action))
		{
			return action;
		}

		return null;
	}

	public Action GetAction(string name)
	{
		int hash = name.GetHashCode();
		Action action;

		if (m_actions.TryGetValue(hash, out action))
		{
			return action;
		}

		return null;
	}
	
	void Flip ()
	{
		m_facingRight = !m_facingRight;

		Vector3 l_scale = transform.localScale;
		l_scale.x *= -1;
		transform.localScale = l_scale;
	}

	/*
	Transform go;
	Transform disabledTexture;
	new SpriteRenderer renderer;
	void UpdateActionBar()
	{
		foreach(KeyValuePair<int,Action> l_action in m_actions)
		{
			go = m_actionBar_Trans.Find ("Action_" + l_action.Key);

			if(go != null)
			{
				Action act = l_action.Value;

				if(act.m_hasTimer)
				{
					disabledTexture = go.Find("DisabledTexture");
					renderer = disabledTexture.GetComponent<SpriteRenderer>();

					if(!act.m_enabled)
					{
						renderer.enabled = true;
						disabledTexture.localScale = 
							new Vector3(
								disabledTexture.localScale.x,
								(1.0f - (act.m_currentTimer/act.m_timerDuration)),
								disabledTexture.localScale.z);
					}
					else
					{
						renderer.enabled = false;
					}
				}

			}

		}
	}*/

	void EnableObstacles()
	{
		m_world.EnableObstacles();
	}

//	void OnGUI()
//	{
//
//		GUIUtils.DrawText("Catiitude Points: " + m_catiitudePoints + "/" + m_world.m_totalPoints,
//		                  10,
//		                  10,
//		                  m_world.m_catiitudePointsStyle,
//		                  false,
//		                  true,
//		                  1,
//		                  Color.black);
//#if UNITY_ANDROID && !UNITY_EDITOR
//		GUIUtils.DrawText("Touch Inputs: " + Input.touchCount,
//		                  10,
//		                  30,
//		                  m_world.m_catiitudePointsStyle,
//		                  false,
//		                  true,
//		                  1,
//		                  Color.black);
//#endif
//	}
	
}
