﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeviceInput : MonoBehaviour 
{
	Touch currentTouch;

	Camera MainCam{get{if(mainCam == null) mainCam = Camera.main; return mainCam;}}
	Camera mainCam;

	Ray ray;
	RaycastHit hit;



	float _horizontal;
	public static float Horizontal
	{
		get{ return instance._horizontal;}

		set
		{
			instance._horizontal = Mathf.Clamp(value, -1f, 1f);
		}
	}

	float _vertical;
	public static float Vertical
	{
		get{ return instance._vertical;}
		
		set
		{
			instance._vertical = Mathf.Clamp(value, -1f, 1f);
		}
	}


	public enum Direction
	{
		LEFT,
		RIGHT,
		UP,
		DOWN,
		NONE
	};

	#region Script Instance
	private static DeviceInput instance = null;
	
	private DeviceInput() { }
	
	public static DeviceInput Instance
	{
		get
		{
			return instance;
		}
	}
	
	void Awake()
	{
		if (DeviceInput.Instance && !(DeviceInput.Instance == this))
			gameObject.SetActive(false);
		else
			instance = this;
	}
	
	#endregion
	
	void Update() 
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidUpdate();
#elif UNITY_EDITOR
		EditorUpdate();

#endif
	}

	Dictionary<int, MoveButton> touchDict = new Dictionary<int, MoveButton>();

	void AndroidUpdate()
	{
		if(Input.touchCount > 0)
		{

			foreach(Touch _tch in Input.touches)
			{
				MoveButton hitButton = null;

				if (_tch.phase == TouchPhase.Began) 
				{
					// Construct a ray from the current touch coordinates
					ray = MainCam.ScreenPointToRay(_tch.position);
					
					//If there was nothing hit, dont bother with anything else
					if(!Physics.Raycast(ray, out hit)) return;
					if(hit.collider == null) return;

					hitButton = hit.collider.GetComponent<MoveButton>();
					hitButton.Press = true;
					
					touchDict.Add(_tch.fingerId, hitButton);
					//TODO Check if we're colliding with the movement buttons
					//TODO Check the button Type
					//TODO Activate Button
					
					
				}
				else if(_tch.phase == TouchPhase.Ended)
				{
					if(!touchDict.TryGetValue(_tch.fingerId, out hitButton)) return;

					hitButton.Press = false;

					hitButton = null;
					touchDict.Remove(_tch.fingerId);
				}
			}
		}
	}
#if UNITY_EDITOR
	MoveButton hitButton;


	void EditorUpdate()
	{
		if (Input.GetMouseButton(0)) 
		{
			
			// Construct a ray from the current touch coordinates
			ray = MainCam.ScreenPointToRay((Vector2)Input.mousePosition);
			
			//If there was nothing hit, dont bother with anything else
			if(!Physics.Raycast(ray, out hit)) return;
			if(hit.collider == null) return;
			
			hitButton = hit.collider.GetComponent<MoveButton>();
			hitButton.Press = true;
			
			
			//TODO Check if we're colliding with the movement buttons
			//TODO Check the button Type
			//TODO Activate Button
			
			
		}
		else if(Input.GetMouseButtonUp(0))
		{
			if(!hitButton) return;
			
			hitButton.Press = false;
			
			hitButton = null;
		}
	}
#endif
		
		
	}
	
class CustomTouch
{
	public MoveButton movebutton;
	public Touch touch;

	public CustomTouch(MoveButton _mb, Touch _t)
	{
		touch = _t;
		movebutton = _mb;
	}

}