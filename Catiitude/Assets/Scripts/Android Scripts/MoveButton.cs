﻿using UnityEngine;
using System.Collections;

public class MoveButton : MonoBehaviour 
{
	public DeviceInput.Direction direction;


	float AxisValue = 0f;
	bool isPressed = false;

	new public SpriteRenderer renderer;
	new Transform transform;
	public bool Press 
	{
		get
		{
			return isPressed;
		} 
		set
		{
			if(value) transform.localScale = new Vector2(0.8f,0.8f);
			else transform.localScale = new Vector2(1f,1f);
			isPressed = value;
		}
	}

	void Start()
	{
		if(!renderer) throw new System.NullReferenceException("No Sprite renderer on MoveButton");
		transform = renderer.gameObject.transform;
	}

	void Update()
	{
		if(AxisValue == 0f && !isPressed) return;
		AxisValue = Mathf.Clamp(
			(isPressed ? (AxisValue += 0.25f) : (AxisValue -= Time.deltaTime * 5f))
			,0f, 1f);

		switch (direction) 
		{
			case DeviceInput.Direction.UP: DeviceInput.Vertical =  AxisValue; break;
			case DeviceInput.Direction.DOWN: DeviceInput.Vertical =  AxisValue * -1f; break;

			case DeviceInput.Direction.LEFT: DeviceInput.Horizontal =  AxisValue * -1f; break;
			case DeviceInput.Direction.RIGHT: DeviceInput.Horizontal =  AxisValue; break;
		}

	}


	/*
	public void Update()
	{
		if(!isPressed)return;

	}
	*/


}
