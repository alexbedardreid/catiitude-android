﻿
using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(UIRepeatButton))]
public class UIRepeatButtonEditor : UIWidgetContainerEditor
{
	SerializedProperty localEnabled;

	public override void OnInspectorGUI()
	{
		/*
		GUILayout.BeginHorizontal();
		GUILayout.Space(25f);
		NGUIEditorTools.SetLabelWidth(95f);
		NGUIEditorTools.DrawProperty("Interval", serializedObject, "Interval", GUILayout.MinWidth(25f), GUILayout.MaxWidth(300f));
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Space(25f);
		NGUIEditorTools.SetLabelWidth(95f);
		NGUIEditorTools.DrawProperty("Click Pause", serializedObject, "ClickPause", GUILayout.MinWidth(25f), GUILayout.MaxWidth(300f));
		GUILayout.EndHorizontal();
		*/

		UIRepeatButton button = target as UIRepeatButton;

		GUILayout.BeginHorizontal();
		localEnabled = NGUIEditorTools.DrawProperty("Notify Custom", serializedObject, "NotifyCustom", GUILayout.Width(100f));
		GUILayout.EndHorizontal();

		if (localEnabled.boolValue == true)
		{
			//this allows to more conveniently set the event handler:
			NGUIEditorTools.DrawEvents("On Repeated Press", button, button.onPressListeners);
		}

	}

	enum Highlight
	{
		DoNothing,
		Press,
	}
}

