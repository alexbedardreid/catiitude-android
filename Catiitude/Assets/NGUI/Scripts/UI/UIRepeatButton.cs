﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIRepeatButton : MonoBehaviour
{
	float Interval = 0.1f;
	float ClickPause = 1f;
    //public float interval = 0.2f;

	public bool NotifyCustom = false;
    
    bool mIsPressed = false;
    float mNextClick = 0f;

    public List<EventDelegate> onPressListeners = new List<EventDelegate>();


	void OnPress(bool isPressed) { mIsPressed = isPressed; if (mIsPressed)mNextClick = Time.realtimeSinceStartup + ClickPause; }

	void OnEnable()//Prevents any "stuck on pressed" type issues when training troops
	{
		if (mIsPressed)
		{
			mIsPressed = false;
			this.GetComponent<UIButton>().isEnabled = false;
		}
	}
    
    void Update ()
    {
        if (mIsPressed && Time.realtimeSinceStartup > mNextClick)
        {
			//Debug.LogError("Active: " + gameObject.activeInHierarchy + ", mIsPressed: " + mIsPressed);

			mNextClick = Time.realtimeSinceStartup + Interval;
            
            // Do what you need to do, or simply:
			if (NotifyCustom) EventDelegate.Execute(onPressListeners);
			else SendMessage("OnClick", SendMessageOptions.DontRequireReceiver);
        }
    }
}
